from flask_wtf import Form
from wtforms import StringField, SubmitField, BooleanField, TextAreaField
from wtforms import DateField, RadioField, IntegerField, SelectField, FieldList
from wtforms import SelectMultipleField, HiddenField
from wtforms.validators import Required, Length, NumberRange, Optional
import json
from time import gmtime, strftime, time

class KeyForm(Form):
    def __init__(self, id):
        super(KeyForm, self).__init__()
        self.id = id

    # Basic Information
    start_time = HiddenField(default=str(int(time())))
    
    # Health services
    aidposthealthservice = StringField('')
    aidposthealthfacility = IntegerField('', validators=[Optional()])
    aidpostpositiondoctors = IntegerField('', validators=[Optional()])
    aidpostpositionnurses = IntegerField('', validators=[Optional()])
    aidpostpositionheos = IntegerField('', validators=[Optional()])
    aidpostpositionchws = IntegerField('', validators=[Optional()])
    aidpostgenderm = IntegerField('', validators=[Optional()])
    aidpostgenderf = IntegerField('', validators=[Optional()])
    aidpostpatientsm = IntegerField('', validators=[Optional()])
    aidpostpatientsf = IntegerField('', validators=[Optional()])
    aidpostfacilitybuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    aidpostelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    healthcenterhealthservice = StringField('')
    healthcenterhealthfacility = IntegerField('', validators=[Optional()])
    healthcenterpositiondoctors = IntegerField('', validators=[Optional()])
    healthcenterpositionnurses = IntegerField('', validators=[Optional()])
    healthcenterpositionheos = IntegerField('', validators=[Optional()])
    healthcenterpositionchws = IntegerField('', validators=[Optional()])
    healthcentergenderm = IntegerField('', validators=[Optional()])
    healthcentergenderf = IntegerField('', validators=[Optional()])
    healthcenterpatientsm = IntegerField('', validators=[Optional()])
    healthcenterpatientsf = IntegerField('', validators=[Optional()])
    healthcenterfacilitybuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    healthcenterelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    hospitalhealthservice = StringField('')
    hospitalhealthfacility = IntegerField('', validators=[Optional()])
    hospitalpositiondoctors = IntegerField('', validators=[Optional()])
    hospitalpositionnurses = IntegerField('', validators=[Optional()])
    hospitalpositionheos = IntegerField('', validators=[Optional()])
    hospitalpositionchws = IntegerField('', validators=[Optional()])
    hospitalgenderm = IntegerField('', validators=[Optional()])
    hospitalgenderf = IntegerField('', validators=[Optional()])
    hospitalpatientsm = IntegerField('', validators=[Optional()])
    hospitalpatientsf = IntegerField('', validators=[Optional()])
    hospitalfacilitybuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    hospitalelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    churchhealthservice = StringField('')
    churchhealthfacility = IntegerField('', validators=[Optional()])
    churchpositiondoctors = IntegerField('', validators=[Optional()])
    churchpositionnurses = IntegerField('', validators=[Optional()])
    churchpositionheos = IntegerField('', validators=[Optional()])
    churchpositionchws = IntegerField('', validators=[Optional()])
    churchgenderm = IntegerField('', validators=[Optional()])
    churchgenderf = IntegerField('', validators=[Optional()])
    churchpatientsm = IntegerField('', validators=[Optional()])
    churchpatientsf = IntegerField('', validators=[Optional()])
    churchfacilitybuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    churchelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    totalhealthservice = StringField('')
    totalhealthfacility = IntegerField('', validators=[Optional()])
    totalpositiondoctors = IntegerField('', validators=[Optional()])
    totalpositionnurses = IntegerField('', validators=[Optional()])
    totalpositionheos = IntegerField('', validators=[Optional()])
    totalpositionchws = IntegerField('', validators=[Optional()])
    totalgenderm = IntegerField('', validators=[Optional()])
    totalgenderf = IntegerField('', validators=[Optional()])
    totalpatientsm = IntegerField('', validators=[Optional()])
    totalpatientsf = IntegerField('', validators=[Optional()])
    totalfacilitybuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    totalelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    # Sickness type
    asthmatype = IntegerField('', validators=[Optional()])
    pneumoniatype = IntegerField('', validators=[Optional()])
    coughtype = IntegerField('', validators=[Optional()])
    flutype = IntegerField('', validators=[Optional()])
    fevertype = IntegerField('', validators=[Optional()])
    bodyachestype = IntegerField('', validators=[Optional()])
    headachetype = IntegerField('', validators=[Optional()])
    tuberculosistype = IntegerField('', validators=[Optional()])
    malariatype = IntegerField('', validators=[Optional()])
    diarreatype = IntegerField('', validators=[Optional()])
    sorestype = IntegerField('', validators=[Optional()])
    othertype = StringField('other, specify')
    othertypenumber = IntegerField('', validators=[Optional()])

    # Education
    eleprepnumberschoolfacility = IntegerField('', validators=[Optional()])
    eleprepnumberteachersfemale = IntegerField('', validators=[Optional()])
    eleprepnumberteachersmale = IntegerField('', validators=[Optional()])
    eleprepnumbergrades = IntegerField('', validators=[Optional()])
    eleprepnumberstudentsfemale = IntegerField('', validators=[Optional()])
    eleprepnumberstudentsmale = IntegerField('', validators=[Optional()])
    eleprepclass1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    eleprepclass1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    eleprepclass2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    eleprepclass2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    eleprepschoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    eleprepelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    eleonenumberschoolfacility = IntegerField('', validators=[Optional()])
    eleonenumberteachersfemale = IntegerField('', validators=[Optional()])
    eleonenumberteachersmale = IntegerField('', validators=[Optional()])
    eleonenumbergrades = IntegerField('', validators=[Optional()])
    eleonenumberstudentsfemale = IntegerField('', validators=[Optional()])
    eleonenumberstudentsmale = IntegerField('', validators=[Optional()])
    eleoneclass1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    eleoneclass1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    eleoneclass2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    eleoneclass2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    eleoneschoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    eleoneelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade3numberschoolfacility = IntegerField('', validators=[Optional()])
    grade3numberteachersfemale = IntegerField('', validators=[Optional()])
    grade3numberteachersmale = IntegerField('', validators=[Optional()])
    grade3numbergrades = IntegerField('', validators=[Optional()])
    grade3numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade3numberstudentsmale = IntegerField('', validators=[Optional()])
    grade3class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade3class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade3class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade3class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade3schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade3electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade4numberschoolfacility = IntegerField('', validators=[Optional()])
    grade4numberteachersfemale = IntegerField('', validators=[Optional()])
    grade4numberteachersmale = IntegerField('', validators=[Optional()])
    grade4numbergrades = IntegerField('', validators=[Optional()])
    grade4numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade4numberstudentsmale = IntegerField('', validators=[Optional()])
    grade4class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade4class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade4class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade4class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade4schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade4electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade5numberschoolfacility = IntegerField('', validators=[Optional()])
    grade5numberteachersfemale = IntegerField('', validators=[Optional()])
    grade5numberteachersmale = IntegerField('', validators=[Optional()])
    grade5numbergrades = IntegerField('', validators=[Optional()])
    grade5numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade5numberstudentsmale = IntegerField('', validators=[Optional()])
    grade5class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade5class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade5class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade5class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade5schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade5electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade6numberschoolfacility = IntegerField('', validators=[Optional()])
    grade6numberteachersfemale = IntegerField('', validators=[Optional()])
    grade6numberteachersmale = IntegerField('', validators=[Optional()])
    grade6numbergrades = IntegerField('', validators=[Optional()])
    grade6numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade6numberstudentsmale = IntegerField('', validators=[Optional()])
    grade6class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade6class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade6class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade6class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade6schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade6electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade7numberschoolfacility = IntegerField('', validators=[Optional()])
    grade7numberteachersfemale = IntegerField('', validators=[Optional()])
    grade7numberteachersmale = IntegerField('', validators=[Optional()])
    grade7numbergrades = IntegerField('', validators=[Optional()])
    grade7numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade7numberstudentsmale = IntegerField('', validators=[Optional()])
    grade7class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade7class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade7class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade7class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade7schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade7electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade8numberschoolfacility = IntegerField('', validators=[Optional()])
    grade8numberteachersfemale = IntegerField('', validators=[Optional()])
    grade8numberteachersmale = IntegerField('', validators=[Optional()])
    grade8numbergrades = IntegerField('', validators=[Optional()])
    grade8numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade8numberstudentsmale = IntegerField('', validators=[Optional()])
    grade8class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade8class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade8class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade8class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade8schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade8electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    totalnumberschoolfacility = IntegerField('', validators=[Optional()])
    totalnumberteachersfemale = IntegerField('', validators=[Optional()])
    totalnumberteachersmale = IntegerField('', validators=[Optional()])
    totalnumbergrades = IntegerField('', validators=[Optional()])
    totalnumberstudentsfemale = IntegerField('', validators=[Optional()])
    totalnumberstudentsmale = IntegerField('', validators=[Optional()])
    totalclass1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    totalclass1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    totalclass2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    totalclass2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    totalschoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    totalelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade9numberschoolfacility = IntegerField('', validators=[Optional()])
    grade9numberteachersfemale = IntegerField('', validators=[Optional()])
    grade9numberteachersmale = IntegerField('', validators=[Optional()])
    grade9numbergrades = IntegerField('', validators=[Optional()])
    grade9numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade9numberstudentsmale = IntegerField('', validators=[Optional()])
    grade9class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade9class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade9class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade9class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade9schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade9electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade10numberschoolfacility = IntegerField('', validators=[Optional()])
    grade10numberteachersfemale = IntegerField('', validators=[Optional()])
    grade10numberteachersmale = IntegerField('', validators=[Optional()])
    grade10numbergrades = IntegerField('', validators=[Optional()])
    grade10numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade10numberstudentsmale = IntegerField('', validators=[Optional()])
    grade10class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade10class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade10class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade10class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade10schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade10electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade11numberschoolfacility = IntegerField('', validators=[Optional()])
    grade11numberteachersfemale = IntegerField('', validators=[Optional()])
    grade11numberteachersmale = IntegerField('', validators=[Optional()])
    grade11numbergrades = IntegerField('', validators=[Optional()])
    grade11numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade11numberstudentsmale = IntegerField('', validators=[Optional()])
    grade11class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade11class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade11class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade11class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade11schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade11electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    grade12numberschoolfacility = IntegerField('', validators=[Optional()])
    grade12numberteachersfemale = IntegerField('', validators=[Optional()])
    grade12numberteachersmale = IntegerField('', validators=[Optional()])
    grade12numbergrades = IntegerField('', validators=[Optional()])
    grade12numberstudentsfemale = IntegerField('', validators=[Optional()])
    grade12numberstudentsmale = IntegerField('', validators=[Optional()])
    grade12class1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    grade12class1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    grade12class2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    grade12class2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    grade12schoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    grade12electricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    totalhighschoolnumberschoolfacility = IntegerField('', validators=[Optional()])
    totalhighschoolnumberteachersfemale = IntegerField('', validators=[Optional()])
    totalhighschoolnumberteachersmale = IntegerField('', validators=[Optional()])
    totalhighschoolnumbergrades = IntegerField('', validators=[Optional()])
    totalhighschoolnumberstudentsfemale = IntegerField('', validators=[Optional()])
    totalhighschoolnumberstudentsmale = IntegerField('', validators=[Optional()])
    totalhighschoolclass1female = RadioField('',
        choices=[('class1 female A', 'A'),
            ('class1 female B', 'B')],
        validators=[Optional()])
    totalhighschoolclass1male = RadioField('',
        choices=[('class1 male A', 'A'),
            ('class1 male B', 'B')],
        validators=[Optional()])
    totalhighschoolclass2female = RadioField('',
        choices=[('class2 female A', 'A'),
            ('class2 female B', 'B')],
        validators=[Optional()])
    totalhighschoolclass2male = RadioField('',
        choices=[('class2 male A', 'A'),
            ('class2 male B', 'B')],
        validators=[Optional()])
    totalhighschoolschoolbuilding = RadioField('',
        choices=[('permanent', '(1) permanent'),
            ('semi-permanent', '(2) semi-permanent'),
            ('impoverished material', '(3) impoverished material')],
        validators=[Optional()])
    totalhighschoolelectricity = RadioField('',
        choices=[('national grid (PPL)', '(1) national grid (PPL)'),
            ('own generator', '(2) own generator')],
        validators=[Optional()])

    # Markets
    nameofmarket1 = StringField('')
    typeofmarket1 = StringField('')
    marketgeneralcomments1 = TextAreaField('')
    nameofmarket2 = StringField('')
    typeofmarket2 = StringField('')
    marketgeneralcomments2 = TextAreaField('')
    nameofmarket3 = StringField('')
    typeofmarket3 = StringField('')
    marketgeneralcomments3 = TextAreaField('')
    nameofmarket4 = StringField('')
    typeofmarket4 = StringField('')
    marketgeneralcomments4 = TextAreaField('')
    nameofmarket5 = StringField('')
    typeofmarket5 = StringField('')
    marketgeneralcomments5 = TextAreaField('')
    nameofmarket6 = StringField('')
    typeofmarket6 = StringField('')
    marketgeneralcomments6 = TextAreaField('')
    typeofmarkettotal = StringField('')
    marketgeneralcommentstotal = TextAreaField('')



    # PMV's
    carnumberofpmvs = IntegerField('', validators=[Optional()])
    carpmvcommentobservation = TextAreaField('')
    seaterbus15numberofpmvs = IntegerField('', validators=[Optional()])
    seaterbus15pmvcommentobservation = TextAreaField('')
    seaterbus25numberofpmvs = IntegerField('', validators=[Optional()])
    seaterbus25pmvcommentobservation = TextAreaField('')
    trucknumberofpmvs = IntegerField('', validators=[Optional()])
    truckpmvcommentobservation = TextAreaField('')
    tractornumberofpmvs = IntegerField('', validators=[Optional()])
    tractorpmvcommentobservation = TextAreaField('')
    totalnumberofpmvs = IntegerField('', validators=[Optional()])
    totalpmvcommentobservation = TextAreaField('')

    # Non-passenger
    carnumberofnonpassenger = IntegerField('', validators=[Optional()])
    carpassengercommentobservation = TextAreaField('')
    trucknumberofnonpassenger = IntegerField('', validators=[Optional()])
    truckpassengercommentobservation = TextAreaField('')
    tractornumberofnonpassenger = IntegerField('', validators=[Optional()])
    tractorpassengercommentobservation = TextAreaField('')
    otherspecifynonpassenger = StringField('Other, Specify')
    othernumberofnonpassenger = IntegerField('', validators=[Optional()])
    otherpassengercommentobservation = TextAreaField('')
    totalnumberofnonpassenger = IntegerField('', validators=[Optional()])
    totalpassengercommentobservation = TextAreaField('')

    # Churches
    romancatholicnumber = IntegerField('', validators=[Optional()])
    evangelicalnumber = IntegerField('', validators=[Optional()])
    unitedchurchnumber = IntegerField('', validators=[Optional()])
    adventistchurchnumber = IntegerField('', validators=[Optional()])
    pentecostalnumber = IntegerField('', validators=[Optional()])
    christiannumber = IntegerField('', validators=[Optional()])
    totalchurchesnumber = IntegerField('', validators=[Optional()])

    # Final Steps
    comments = TextAreaField('Write down below any comments you have.', render_kw={"rows": 2, "cols": 80})
    confirmation = BooleanField('Confirm that you have finished entering all the information before submitting',
        validators=[Required()])

    submit = SubmitField('Submit')
