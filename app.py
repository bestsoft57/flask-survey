from flask import Flask, render_template, redirect, url_for, session, request, flash
from flask_bootstrap import Bootstrap
from household import HouseholdForm
from ftrade_store import FinalTradeStoreForm
from pmv import PMVForm
from fsv_men import FSVMen
from fsv_women import FSVWomen
from key import KeyForm
import json
import datetime
import webbrowser
import string
import random
from time import gmtime, strftime, time

def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.SystemRandom().choice(chars) for _ in range(size))

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, datetime.date):
        serial = obj.isoformat()
        return serial
    raise TypeError ("Type not serializable", obj)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'somesecretkeyhere'
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
bootstrap = Bootstrap(app)
url = 'http://localhost:5000/householdsurvey'

@app.route('/')
@app.route('/home')
def index():
    print(locals())
    return render_template('index.html')

@app.route('/success')
def success():
    print(locals())
    return render_template('index-success.html')

@app.route('/householdsurvey', methods=['GET', 'POST'])
def householdsurvey():
    if 'survey1id' not in session:
        session['survey1id'] = id_generator()
    form = HouseholdForm(id = session['survey1id'])

    if form.validate_on_submit():
        hh_data = form.data.copy()
        # hh_data['end_time'] = str(int(time()))

        # hh_data = householdsurveyother(hh_data)
        
        with open('../data/HH-' + session['survey1id'] + strftime("-%Y-%m-%d--%H-%M-%S", gmtime()) + '.json', 'w') as fp:
            json.dump([hh_data], fp, default=json_serial)
        session.pop('survey1id')
        flash('Survery submitted!')
        return redirect(url_for('index'))
    if request.method == 'GET':
        form.start_time.data = str(int(time()))
    return render_template('hh-survey.html', form=form)

def householdsurveyother(hh_data):
    if hh_data['expendituremoney'] == "increase":
        hh_data['expendituremoney'] = request.form.get('expendituremoneyincrease')

    if hh_data['expendituremoney'] == "decrease":
        hh_data['expendituremoney'] = request.form.get('expendituremoneydecrease')

@app.route('/pmv', methods=['GET', 'POST'])
def pmv():
    if 'survey2id' not in session:
        session['survey2id'] = id_generator()
    form = PMVForm(id = session['survey2id'])
    if form.validate_on_submit():
        pmv_data = form.data.copy()
        # pmv_data['end_time'] = str(int(time()))
        
        with open('../data/PMV-' + session['survey2id'] + strftime("-%Y-%m-%d--%H-%M-%S", gmtime()) + '.json', 'w') as fp:
            json.dump([pmv_data], fp, default=json_serial)
        session.pop('survey2id')
        flash('Survery submitted!')
        return redirect(url_for('index'))
    if request.method == 'GET':
        form.start_time.data = str(int(time()))
    return render_template('pmv-survey.html', form=form)


@app.route('/finaltradestore', methods=['GET', 'POST'])
def finaltradestore():
    if 'survey2id' not in session:
        session['survey2id'] = id_generator()
    form = FinalTradeStoreForm(id = session['survey2id'])
    if form.validate_on_submit():
        ftstore_data = form.data.copy()
        # ftstore_data['end_time'] = str(int(time()))
        
        with open('../data/FTSTORE-' + session['survey2id'] + strftime("-%Y-%m-%d--%H-%M-%S", gmtime()) + '.json', 'w') as fp:
            json.dump([ftstore_data], fp, default=json_serial)
        session.pop('survey2id')
        flash('Survery submitted!')
        return redirect(url_for('index'))
    if request.method == 'GET':
        form.start_time.data = str(int(time()))
    return render_template('ftrade-survey.html', form=form)


@app.route('/fsv-women', methods=['GET', 'POST'])
def fsv_women():
    if 'survey3id' not in session:
        session['survey3id'] = id_generator()
    form = FSVWomen(id = session['survey3id'])
    if form.validate_on_submit():
        fsv_women_data = form.data.copy()
        fsv_women_data['end_time'] = str(int(time()))
        with open('../data/FSV/Women-' + session['survey3id'] + strftime("-%Y-%m-%d--%H-%M-%S", gmtime()) + '.json', 'w') as fp:
            json.dump([fsv_women_data], fp, default=json_serial)
        session.pop('survey3id')
        return redirect(url_for('success'))
    if request.method == 'GET':
        form.start_time.data = str(int(time()))
    return render_template('fsv-women.html', form=form)

@app.route('/fsv-men', methods=['GET', 'POST'])
def fsv_men():
    if 'survey4id' not in session:
        session['survey4id'] = id_generator()
    form = FSVMen(id = session['survey4id'])
    if form.validate_on_submit():
        fsv_men_data = form.data.copy()
        fsv_men_data['end_time'] = str(int(time()))
        with open('../data/FSV/Men-' + session['survey4id'] + strftime("-%Y-%m-%d--%H-%M-%S", gmtime()) + '.json', 'w') as fp:
            json.dump([fsv_men_data], fp, default=json_serial)
        session.pop('survey4id')
        return redirect(url_for('success'))
    if request.method == 'GET':
        form.start_time.data = str(int(time()))
    return render_template('fsv-men.html', form=form)

@app.route('/key-informant', methods=['GET', 'POST'])
def key_informant():
    if 'survey2id' not in session:
        session['survey2id'] = id_generator()
    form = KeyForm(id = session['survey2id'])
    if form.validate_on_submit():
        pmv_data = form.data.copy()
        # pmv_data['end_time'] = str(int(time()))
        
        with open('../data/Key-' + session['survey2id'] + strftime("-%Y-%m-%d--%H-%M-%S", gmtime()) + '.json', 'w') as fp:
            json.dump([pmv_data], fp, default=json_serial)
        session.pop('survey2id')
        flash('Survery submitted!')
        return redirect(url_for('index'))
    if request.method == 'GET':
        form.start_time.data = str(int(time()))
    return render_template('key-survey.html', form=form)

webbrowser.open(url)

if __name__ == '__main__':
    app.run(debug=True)
