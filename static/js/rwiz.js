function otherFocus(element) {
    document.getElementById(element).checked = true;
}

$(document).ready(function () {
    $("input:radio[name='usualcargo']").change(function () {
        var target = $(this).data('target'),
            option = $(this).data('option')

        if (option == "enable") {
            $(`#${target}`).prop('disabled', false)
        } else {
            $(`#${target}`).prop('disabled', true)
            $(`#${target}`).val('')
        }
    })

    $('input:radio[name="setfares"]').change(function () {
        var target = $(this).data('target'),
            option = $(this).data('option')

        if (option == "enable") {
            // $(`#${target}`).prop('disabled', false)
            $('input:radio[name="faresetter"]').prop('disabled', false)
            // $('textarea[name="faresetterother"]').prop('disabled', false)
        } else {
            // $(`#${target}`).prop('disabled', true)
            $('input:radio[name="faresetter"]').prop('disabled', true)
            $('textarea[name="faresetterother"]').prop('disabled', true)
            $('input:radio[name="faresetter"]').prop('checked', false)
            $('textarea[name="faresetterother"]').val('')
        }
    })

    $('input:radio[name="faresetter"]').change(function () {
        var target = $(this).data('target'),
            option = $(this).data('option')

        if (option == "enable") {
            $(`#${target}`).prop('disabled', false)
        } else {
            $(`#${target}`).prop('disabled', true)
            $(`#${target}`).val('')

        }
    })

    $('input:radio[name="pmvstopinrainyseason"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            // $(`#${target}`).prop('disabled', false)
            $("input:text[name='pmvstopinrainyseasonmonths']").prop('disabled', false)
            $("input:text[name='pmvstopinrainyseasonreason']").prop('disabled', false)
        } else {
            // $(`#${target}`).prop('disabled', true)
            $("input:text[name='pmvstopinrainyseasonmonths']").prop('disabled', true)
            $("input:text[name='pmvstopinrainyseasonreason']").prop('disabled', true)

            $("input:text[name='pmvstopinrainyseasonmonths']").val('')
            $("input:text[name='pmvstopinrainyseasonreason']").val('')
        }
    })

    $('input:radio[name="changesintrafficaccidents"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            // $(`#${target}`).prop('disabled', false)
            $("#changesintrafficaccidentsfrequency-0").prop('disabled', false)
            $("#changesintrafficaccidentsfrequency-1").prop('disabled', false)
            $("#changesintrafficaccidentsfrequency-2").prop('disabled', false)
        } else {
            // $(`#${target}`).prop('disabled', true)
            $("#changesintrafficaccidentsfrequency-0").prop('disabled', true)
            $("#changesintrafficaccidentsfrequency-1").prop('disabled', true)
            $("#changesintrafficaccidentsfrequency-2").prop('disabled', true)

            $("input:radio[name='changesintrafficaccidentsfrequency']").prop('checked', false)
        }
    })

    $('input:radio[name="changestohouse"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            // $(`#${target}`).prop('disabled', false)
            $("#changestohousedetails").prop('disabled', false)
            $("#changestohouseyear").prop('disabled', false)
        } else {
            // $(`#${target}`).prop('disabled', true)
            $("#changestohousedetails").prop('disabled', true)
            $("#changestohouseyear").prop('disabled', true)

            $("#changestohouseyear").val('')
            $("#changestohousedetails").val('')
        }
    })

    $('input:radio[name="houseelectricity"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('input:radio[name="houseelectricitytype"]').prop('disabled', false)
        } else {
            $('input:radio[name="houseelectricitytype"]').prop('disabled', true)
            $('#houseelectricitytypeother').prop('disabled', true)

            $('input:radio[name="houseelectricitytype"]').prop('checked', false)
            $('#houseelectricitytypeother').val('')
        }
    })

    $('input:radio[name="sellanimals"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#sellanimalsother').prop('disabled', false)
        } else {
            $('#sellanimalsother').prop('disabled', true)
            $('#sellanimalsother').val('')
        }
    })

    $('input:radio[name="havesoilerosion"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#reasonforerosion').prop('disabled', false)
        } else {
            $('#reasonforerosion').prop('disabled', true)
            $('#reasonforerosion').val('')
        }
    })

    $('input:radio[name="traderscometothisvillage"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('input:radio[name="satisfiedwithpricesoffered"]').prop('disabled', false)
            $('#traderscometothisvillageyes').prop('disabled', false)
            $('#traderscometothisvillagehowoften').prop('disabled', false)
        } else {
            $('input:radio[name="satisfiedwithpricesoffered"]').prop('disabled', true)
            $('#traderscometothisvillageyes').prop('disabled', true)
            $('#traderscometothisvillagehowoften').prop('disabled', true)

            $('input:radio[name="satisfiedwithpricesoffered"]').prop('checked', false)
            $('#traderscometothisvillageyes').val('')
            $('#traderscometothisvillagehowoften').val('')
        }
    })

    $('input:radio[name="changestocrops"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            
            $('#changestocropsdetails').prop('disabled', false)
            
        } else {
            
            $('#changestocropsdetails').prop('disabled', true)
            $('#changestocropsdetails').val('')
        }
    })

    $('input:radio[name="changestonumberofpeopleinlivestockactivities"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {

            $('#changestonumberofpeopleinlivestockactivitiesdetails').prop('disabled', false)
        } else {

            $('#changestonumberofpeopleinlivestockactivitiesdetails').prop('disabled', true)
            $('#changestonumberofpeopleinlivestockactivitiesdetails').val('')
        }
    })

    $('input:radio[name="hhtoilet"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('input:radio[name="hhkindoftoilet"]').prop('disabled', false)

            $('#hhmemberfacilityother').prop('disabled', true)
            $('#hhmemberfacilityother').val('')
            $('input:radio[name="hhmemberfacility"]').prop('checked', false)
            $('input:radio[name="hhmemberfacility"]').prop('disabled', true)
        } else {
            $('#hhkindoftoiletother').prop('disabled', true)
            $('#hhkindoftoiletother').val('')
            $('input:radio[name="hhkindoftoilet"]').prop('checked', false)
            $('input:radio[name="hhkindoftoilet"]').prop('disabled', true)
            
            $('input:radio[name="hhmemberfacility"]').prop('disabled', false)
        }
    })

    $('input:radio[name="schoolagednotinschoolchildrennow"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#schoolagednotinschoolgirlsnow').prop('disabled', false)
            $('#schoolagednotinschoolboysnow').prop('disabled', false)
        } else {
            $('#schoolagednotinschoolgirlsnow').prop('disabled', true)
            $('#schoolagednotinschoolboysnow').prop('disabled', true)

            $('#schoolagednotinschoolgirlsnow').val('')
            $('#schoolagednotinschoolboysnow').val('')
        }
    })

    $('input:radio[name="hhclassificationdifferentbefore"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#hhclassificationdifferentbeforedetails').prop('disabled', false)
        } else {
            $('#hhclassificationdifferentbeforedetails').prop('disabled', true)
            $('#hhclassificationdifferentbeforedetails').val('')
        }
    })

    $('input:radio[name="villageclassificationbefore"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#villageclassificationbeforedetails').prop('disabled', false)
        } else {
            $('#villageclassificationbeforedetails').prop('disabled', true)
            $('#villageclassificationbeforedetails').val('')
        }
    })

    $('input:radio[name="waterqualitychanged"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#waterqualitychangedreasons').prop('disabled', false)
        } else {
            $('#waterqualitychangedreasons').prop('disabled', true)
            $('#waterqualitychangedreasons').val('')
        }
    })

    $('input:radio[name="airqualitychanged"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#airqualitychangedreasons').prop('disabled', false)
        } else {
            $('#airqualitychangedreasons').prop('disabled', true)
            $('#airqualitychangedreasons').val('')
        }
    })

    $('input:radio[name="changeinroadsidevegetation"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#changeinroadsidevegetationreasons').prop('disabled', false)
        } else {
            $('#changeinroadsidevegetationreasons').prop('disabled', true)
            $('#changeinroadsidevegetationreasons').val('')
        }
    })

    $('input:radio[name="changeinwildanimal"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#changeinwildanimalreasons').prop('disabled', false)
        } else {
            $('#changeinwildanimalreasons').prop('disabled', true)
            $('#changeinwildanimalreasons').val('')
        }
    })

    $('input:radio[name="trafficmovementproducedust"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('input:radio[name="dustlevelofroad"]').prop('disabled', false)
            $('input:radio[name="dustlevelofroad"]').prop('checked', false)
        } else {
            $('input:radio[name="dustlevelofroad"]').prop('disabled', true)
            $('input:radio[name="dustlevelofroad"]').prop('checked', false)
        }
    })

    $('input:radio[name="paidmaintenance"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#whopaymaintenance').prop('disabled', false)
        } else {
            $('#whopaymaintenance').prop('disabled', true)
            $('#whopaymaintenance').val('')
        }
    })

    $('input:radio[name="womenworkonroadmaintenance"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#howmanywomenmaintain').prop('disabled', false)
        } else {
            $('#howmanywomenmaintain').prop('disabled', true)
            $('#howmanywomenmaintain').val('')
        }
    })

    $('input:radio[name="migratedatetolocation"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#migratewherewhen').prop('disabled', false)
        } else {
            $('#migratewherewhen').prop('disabled', true)
            $('#migratewherewhen').val('')
        }
    })

    $('input:radio[name="respondentmainjob"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#respondentmainjobother').prop('disabled', false)
        } else {
            $('#respondentmainjobother').prop('disabled', true)
            $('#respondentmainjobother').val('')
        }
    })

    $('input:radio[name="spousemainjob"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#spousemainjobother').prop('disabled', false)
        } else {
            $('#spousemainjobother').prop('disabled', true)
            $('#spousemainjobother').val('')
        }
    })

    $('input:radio[name="houseownership"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#houseownershipother').prop('disabled', false)
        } else {
            $('#houseownershipother').prop('disabled', true)
            $('#houseownershipother').val('')
        }
    })

    $('input:radio[name="landownership"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#landownershipother').prop('disabled', false)
        } else {
            $('#landownershipother').prop('disabled', true)
            $('#landownershipother').val('')
        }
    })

    $('input:radio[name="typeofhouse"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#typeofhouseother').prop('disabled', false)
        } else {
            $('#typeofhouseother').prop('disabled', true)
            $('#typeofhouseother').val('')
        }
    })

    $('input:radio[name="houseelectricitytype"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#houseelectricitytypeother').prop('disabled', false)
        } else {
            $('#houseelectricitytypeother').prop('disabled', true)
            $('#houseelectricitytypeother').val('')
        }
    })

    $('input:radio[name="hhkindoftoilet"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#hhkindoftoiletother').prop('disabled', false)
        } else {
            $('#hhkindoftoiletother').prop('disabled', true)
            $('#hhkindoftoiletother').val('')
        }
    })

    $('input:radio[name="hhmemberfacility"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#hhmemberfacilityother').prop('disabled', false)
        } else {
            $('#hhmemberfacilityother').prop('disabled', true)
            $('#hhmemberfacilityother').val('')
        }
    })

    $('input:radio[name="schoolagednotinschoolreasonnow"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#schoolagednotinschoolreasonnowother').prop('disabled', false)
        } else {
            $('#schoolagednotinschoolreasonnowother').prop('disabled', true)
            $('#schoolagednotinschoolreasonnowother').val('')
        }
    })

    $('input:radio[name="howdmanydaysfoodsupplies"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#howdmanydaysfoodsuppliesother').prop('disabled', false)
        } else {
            $('#howdmanydaysfoodsuppliesother').prop('disabled', true)
            $('#howdmanydaysfoodsuppliesother').val('')
        }
    })

    $('input:radio[name="tradestoresinfrontage"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#tradestoresinfrontageother').prop('disabled', false)
        } else {
            $('#tradestoresinfrontageother').prop('disabled', true)
            $('#tradestoresinfrontageother').val('')
        }
    })

    $('input:text[name="roadtomovegoodstotrade"]').on('input', function () {
        var value = $(this).val()

        if (value != "") {
            $('#roadforeasymovegoods').prop('disabled', false)
        } else {
            $('#roadforeasymovegoods').prop('disabled', true)
            $('#roadforeasymovegoods').val('')
        }
    })

    $('input:radio[name="changesinanswertoquestion"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#changesinanswertoquestionother').prop('disabled', false)
        } else {
            $('#changesinanswertoquestionother').prop('disabled', true)
            $('#changesinanswertoquestionother').val('')
        }
    })

    $('input:radio[name="moveotherstuff"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#moveotherstuffother').prop('disabled', false)
        } else {
            $('#moveotherstuffother').prop('disabled', true)
            $('#moveotherstuffother').val('')
        }
    })

    $('input:radio[name="averageincomechangeddetail"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#averageincomechangeddetailother').prop('disabled', false)
        } else {
            $('#averageincomechangeddetailother').prop('disabled', true)
            $('#averageincomechangeddetailother').val('')
        }
    })

    $('input:radio[name="commoditiesresaleitems"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#whatitemcollected').prop('disabled', false)
            $('#traderoutside').prop('disabled', false)
            $('#commdestimatequantity').prop('disabled', false)
            $('#whenproductscollected').prop('disabled', false)
        } else {
            $('#whatitemcollected').prop('disabled', true)
            $('#whatitemcollected').val('')
            $('#traderoutside').prop('disabled', true)
            $('#traderoutside').val('')
            $('#commdestimatequantity').prop('disabled', true)
            $('#commdestimatequantity').val('')
            $('#whenproductscollected').prop('disabled', true)
            $('#whenproductscollected').val('')
        }
    })

    $('input:radio[name="tradestorelocalproduced"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#tradestorelocalproducedother').prop('disabled', false)
        } else {
            $('#tradestorelocalproducedother').prop('disabled', true)
            $('#tradestorelocalproducedother').val('')
        }
    })

    $('input:radio[name="changestonumberofcustomer"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#changestonumberofcustomerother').prop('disabled', false)
        } else {
            $('#changestonumberofcustomerother').prop('disabled', true)
            $('#changestonumberofcustomerother').val('')
        }
    })

    $('input:radio[name="repaircostschangein10years"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#repaircostschangein10yearsdetails').prop('disabled', false)
        } else {
            $('#repaircostschangein10yearsdetails').prop('disabled', true)
            $('#repaircostschangein10yearsdetails').val('')
        }
    })

    $('input:radio[name="safetyofpassenger"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#safetyofpassengerdetails').prop('disabled', false)
        } else {
            $('#safetyofpassengerdetails').prop('disabled', true)
            $('#safetyofpassengerdetails').val('')
        }
    })

    $('input:radio[name="drivesomeotherroad"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#drivesomeotherroadother1').prop('disabled', false)
            $('#drivesomeotherroadother2').prop('disabled', false)
            $('#roadtoseerepaired').prop('disabled', false)
            $('#roadtoseerepairedwhy').prop('disabled', false)
        } else {
            $('#drivesomeotherroadother1').prop('disabled', true)
            $('#drivesomeotherroadother1').val('')
            $('#drivesomeotherroadother2').prop('disabled', true)
            $('#drivesomeotherroadother2').val('')
            $('#roadtoseerepaired').prop('disabled', true)
            $('#roadtoseerepaired').val('')
            $('#roadtoseerepairedwhy').prop('disabled', true)
            $('#roadtoseerepairedwhy').val('')
        }
    })

    $('input:radio[name="pmvproblemspresentroadconditions"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#pmvproblemspresentroadconditionsother').prop('disabled', false)
        } else {
            $('#pmvproblemspresentroadconditionsother').prop('disabled', true)
            $('#pmvproblemspresentroadconditionsother').val('')
        }
    })

    $('input:radio[name="fatalitiesaware"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#injuredmalenumber').prop('disabled', false)
            $('#injuredfemalenumber').prop('disabled', false)
        } else {
            $('#injuredmalenumber').prop('disabled', true)
            $('#injuredmalenumber').val('')
            $('#injuredfemalenumber').prop('disabled', true)
            $('#injuredfemalenumber').val('')
        }
    })

    $('input:radio[name="howmanypmvsownedin10years"]').change(function () {
        var option = $(this).data('option')

        if (option == "enable") {
            $('#howmanypmvsownedin10yearsother').prop('disabled', false)
        } else {
            $('#howmanypmvsownedin10yearsother').prop('disabled', true)
            $('#howmanypmvsownedin10yearsother').val('')
        }
    })

})