from flask_wtf import Form
from wtforms import StringField, SubmitField, BooleanField, TextAreaField
from wtforms import DateField, RadioField, IntegerField, SelectField, FieldList
from wtforms import SelectMultipleField, FloatField, HiddenField
from wtforms.validators import Required, Length, NumberRange, Optional
import json
from time import gmtime, strftime, time

class PMVForm(Form):
    def __init__(self, id):
        super(PMVForm, self).__init__()
        self.id = id

    start_time = HiddenField(default=str(int(time())))
    dateofinterview = DateField('Date of the interview (e.g., 2016-01-31)')
    namephoneofstorekeeper = StringField('Name and phone number of PMV operator:', validators=[Length(3, 100)])
    interviewer = StringField('Name of the interviewer:')
    roadcorridor = StringField('Road Corridor')
    villagename = StringField('Name of Village and Province')

    origin = StringField('Origin ')
    destination = StringField('Destination ')
    totallengthroadpmv = IntegerField('Total length of the road you operate PMV ', validators=[Optional()])

    averageweeklytrips = IntegerField('2. How many round-trips you have made on this road during last week?', validators=[Optional()])

    frequencydrymonths = IntegerField('a) dry months?', validators=[Optional()])
    frequencywetmonths = IntegerField('and b) wet months?', validators=[Optional()])
    
    traveltimedrymonths = IntegerField('dry', validators=[Optional()])
    traveltimewetmonths = IntegerField('wet', validators=[Optional()])
    
    totallengthoneway = IntegerField('5. What is the total length of the route (one-way) you operate PMV?', validators=[Optional()])

    typeofvehicle = RadioField('6. What type of vehicle do you drive?',
        choices=[('car', 'a) Car'),
        ('15 seater bus', 'b) 15 seater bus'),
        ('25 seater bus', 'c) 25 seater bus'),
        ('truck', 'd) Truck (dyna)'),
        ('tracktor', 'e) Tractor')], validators=[Optional()])

    vehicleownership = RadioField('8. You are',
        choices=[('the owner of the vehicle', 'a) the owner of the vehicle'),
        ('renter of the vehicle', 'b) renter of the vehicle'),
        ('driving a vehicle hired by the government', 'c) driving a vehicle hired by the government'),
        ('work for government', 'd) work for government'),
        ('hired to drive vehicle (private sector)', 'e) hired to drive vehicle (private sector)')], validators=[Optional()])
    howmanypmvsowned = IntegerField('9. If you are owner of the PMV, how many other PMVs do you own?', validators=[Optional()])
    howmanypmvsownedin10years = RadioField('10. Is your PMV leased?', 
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    howmanypmvsownedin10yearsother = TextAreaField('If yes how much leasing fees do you pay per month?', render_kw={"rows": 2, "cols": 80})


    pmvrepairspayer = RadioField('11. Who usually pays for the vehicle repair – you or owner?',
        choices=[('driver', 'Driver'), ('owner', 'Owner')], validators=[Optional()])
    repaircostspermonth = IntegerField('a. Per month kina', validators=[Optional()])
    repaircostsperyear = IntegerField('b. Per year kina', validators=[Optional()])
    repaircostschangein10years = RadioField('13. Has there been any changes in repair cost during recent years?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    repaircostschangein10yearsdetails = TextAreaField('If yes, please specify.', render_kw={"rows": 2, "cols": 80})

    repiaraveragemonth = IntegerField('14. How much do you pay on average/month for repair ?', validators=[Optional()])

    tyrerepaircostspermonth = IntegerField('a) Per month kina', validators=[Optional()])
    tyrerepaircostsperyear = IntegerField('b) Per year kina', validators=[Optional()])
    
    fuelcostsperday = IntegerField('16. What is the fuel cost/day?', validators=[Optional()])

    usualcargo = RadioField('17. What type of cargo do you usually carry?',
        choices=[('passengers', 'a) Passengers'),
        ('food', 'b) Food'),
        ('nonfood', 'c) Nonfood')], validators=[Optional()])
    
    usualcargoother = TextAreaField('please specify', render_kw={"rows": 2, "cols": 80})
    averagedisitance = IntegerField('18. What is the average distance your vehicle travels/day?', validators=[Optional()])
    minimumfare = IntegerField('19. What is the minimum passenger fare you charge per kilometer?', validators=[Optional()])
    maximumfare = IntegerField('20. What is the maximum fare you charge per passenger?', validators=[Optional()])

    freightfeenow = IntegerField('21. What is the freight fee per kilometer you charge on your most frequent route?', validators=[Optional()])

    setfares = RadioField('22. How are passenger fares set? Does someone or organization set your fares or freight rates?',
        choices=[ ('yes', 'a) Yes'), ('no', 'b) No')], validators=[Optional()])
    faresetter = RadioField('If yes:',
        choices=[('owner', 'a) set by Owner'),
        ('other', 'b) set by other')], validators=[Optional()])

    faresetterother = TextAreaField('Other, specify', render_kw={"rows": 2, "cols": 80})

    safetyofpassenger = RadioField('23. Do you ensure the safety of passengers especially females and children when drunkards travel in PMV?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    safetyofpassengerdetails = TextAreaField('If Yes, in what way?', render_kw={"rows": 2, "cols": 80})

    numberofpassengers = IntegerField('24. If you carry passengers, how many on average per trip?', validators=[Optional()])

    drivesomeotherroad = RadioField('25. Do you drive on some other roads (not the road you usually use) to access to villages?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    drivesomeotherroadother1 = TextAreaField('a) if yes, specify which roads', render_kw={"rows": 2, "cols": 80})
    drivesomeotherroadother2 = TextAreaField('b) to which villages', render_kw={"rows": 2, "cols": 80})

    roadtoseerepaired = TextAreaField('26. if yes to 25, why? Which of these roads would you like to see repaired?', render_kw={"rows": 3, "cols": 80})
    roadtoseerepairedwhy = TextAreaField('Why?', render_kw={"rows": 2, "cols": 80})

    carquantity = IntegerField('a) Car', validators=[Optional()])
    bus15seaterquantity = IntegerField('b) 15-seater bus', validators=[Optional()])
    bus25seaterquantity = IntegerField('c) 25-seater bus', validators=[Optional()])
    lighttruckquantity = IntegerField('d) Light Truck', validators=[Optional()])
    mediumtruckquantity = IntegerField('e) Medium Truck', validators=[Optional()])  
    heavytruckquantity = IntegerField('f) Heavy Truck', validators=[Optional()])
    tractorquantity = IntegerField('g) Tractor', validators=[Optional()])

    changesinpmvoperations = TextAreaField('28. Are there changes in the number and quality of PMV operations in the past few years? <br />Specify and give reasons', render_kw={"rows": 2, "cols": 80})

    earningnow = IntegerField('29. How much do you earn per month by operating this vehicle?', validators=[Optional()])

    pmvstopinrainyseason = RadioField('30. Does PMV service stop in the rainy season?',
        choices=[('yes', 'Yes'), ('no', 'No')], validators=[Optional()])

    pmvstopinrainyseasonmonths = IntegerField('If yes, for how many months?', validators=[Optional()])
    pmvstopinrainyseasonreason = StringField('and why?')

    passableroadpercentage = IntegerField('31. Can you please try to estimate the percentage of the navigable road length', validators=[Optional()])
    unpassableroadpercentage = IntegerField('and not navigable', validators=[Optional()])

    pmvproblemspresentroadconditions = RadioField('32. Do you have problems of PMV operation due to present road conditions?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    pmvproblemspresentroadconditionsother = TextAreaField('If yes, please specify', render_kw={"rows": 2, "cols": 80})
    pmvproblemsbeforeworks = TextAreaField('33. Can you tell what kind of impacts of road rehabilitation have had on your PMV operation (in terms of travel time, repair cost, number of passengers take in, frequency of operations)?', render_kw={"rows": 3, "cols": 80})

    changesintrafficaccidents = RadioField('34. Comparing before and after road rehabilitation, can you see changes in traffic accidents?',
        choices=[('yes', 'Yes'), ('no', 'No')], validators=[Optional()])
    changesintrafficaccidentsfrequency = RadioField('If yes, are they',
        choices=[('frequent', 'a) frequent'),
        ('less frequent', 'b) less frequent'),
        ('never heard in this village', 'c) never heard in this village')],
        validators=[Optional()])
    fatalitiesaware = RadioField('35. If yes are you aware of any fatalities because of accidents?',
        choices=[('yes', 'Yes'), ('no', 'No')], validators=[Optional()])
    injuredmalenumber = IntegerField('If yes, how many injured people because of the accident in the past year?', validators=[Optional()])
    injuredfemalenumber = IntegerField('and how many female injured?', validators=[Optional()])

    mainreasonforaccidents = RadioField('36. If there have been accidents (either now or past) what has been the main reason for the accident (your opinion)?',
        choices=[('speeding', 'a. speeding'),
        ('reckless driving', 'b. reckless driving'),
        ('driven under the influence of alcohol', 'c. driven under the influence of alcohol'),
        ('bad/poor road condition', 'd. bad/poor road condition')], validators=[Optional()])

    # Final Steps
    comments = TextAreaField('Write down below any comments you have.', render_kw={"rows": 2, "cols": 80})
    confirmation = BooleanField('Confirm that you have finished entering all the information before submitting',
        validators=[Required()])

    submit = SubmitField('Submit')
