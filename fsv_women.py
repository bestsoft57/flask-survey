from flask_wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, BooleanField
from wtforms import DateField, RadioField, IntegerField, SelectField, FieldList
from wtforms import SelectMultipleField, FloatField, HiddenField
from wtforms.validators import Required, Length, NumberRange, Optional
import json
from time import gmtime, strftime, time

class FSVWomen(Form):
    def __init__(self, id):
        super(FSVWomen, self).__init__()
        self.id = id

    # Basic Information
    start_time = HiddenField(default=str(int(time())))
    province = StringField('PROVINCE')
    district = StringField('DISTRICT LLG Name')
    wardname = StringField('WARD Name/Number')
    villagename = StringField('VILLAGE Name', validators=[Optional()])
    householdnumber = IntegerField('Household Number', validators=[Optional()])
    interviewer = RadioField('Interviewer',
        choices=[('Jackson', 'Jackson'),
            ('Joshua', 'Joshua'),
            ('Katherine', 'Katherine'),
            ('Kolis', 'Kolis'),
            ('Michael', 'Michael'),
            ('Stella', 'Stella')])
    dateofinterview = DateField('Date of the Interview (e.g., 2016-01-31)')
    maritalstatus = RadioField('1. Marital status',
        choices=[('single', 'single'),
            ('married', 'married'),
            ('married but living separately', 'married but living separately'),
            ('divorced', 'divorced'),
            ('widowed', 'widowed'),
            ('never married', 'never married')], validators=[Optional()])
    spousegirlfriend = RadioField('2. Does your spouse have another girlfriend/wife?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    ageoftherespondent = IntegerField('3. Age of the respondent (please note, the respondent must be over 18 years old)', validators=[Optional()])
    literacy = RadioField('4. Literacy',
        choices=[('local dialect', 'local dialect'),
            ('English', 'English'),
            ('Pidgin', 'Pidgin')], validators=[Optional()])
    mainjob = RadioField('5. Main job of the respondent',
        choices=[('subsistence farmer', 'subsistence farmer'),
            ('store vendor', 'store vendor'),
            ('mining company', 'mining company'),
            ('private sector employee', 'private sector employee'),
            ('health worker', 'health worker')], validators=[Optional()])
    mainjobother = TextAreaField('other, specify')
    mainjobspouse = RadioField('6. Spouse\'s main job',
        choices=[('subsistence farmer', 'subsistence farmer'),
            ('store vendor', 'store vendor'),
            ('mining company', 'mining company'),
            ('private sector employee', 'private sector employee'),
            ('health worker', 'health worker')], validators=[Optional()])
    mainjobspouseother = TextAreaField('other, specify')
    miningforalluvialminerals = RadioField('7. Do you or does your spouse mine for alluvial minerals? ',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    respondenteducation = RadioField('8. Education level of the respondent',
        choices=[('never gone to school', 'never gone to school'),
            ('primary education', 'primary education (grades 1-8)'),
            ('secondary school education', 'secondary school education'),
            ('college educational qualification ', 'college educational qualification '),
            ('university educational', 'university educational'),
            ('other', 'other')], validators=[Optional()])
    spouseeducation = RadioField('9. Education level of spouse',
        choices=[('never gone to school', 'never gone to school'),
            ('primary education', 'primary education (grades 1-8)'),
            ('secondary school education', 'secondary school education'),
            ('college educational qualification ', 'college educational qualification '),
            ('university educational', 'university educational'),
            ('other', 'other')], validators=[Optional()])
    statusincommunity = RadioField('10. Your status in the community',
        choices=[('elder/chief', 'elder/chief'),
            ('community leader', 'community leader'),
            ('government representative', 'government representative'),
            ('ordinary community member ', 'ordinary community member ')], validators=[Optional()])
    numberofchildren = IntegerField('11. Number of children in the house you live', validators=[Optional()])
    numberofboys = IntegerField('12. Number of boys in the house you live', validators=[Optional()])
    numberofgirls = IntegerField('13. Number of girls in the house you live', validators=[Optional()])
    familyincome = IntegerField('14. Family income (Kina per month)', validators=[Optional()])
    migrated = RadioField('15. Did you migrate to your present location from somewhere?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    migratedwhen = TextAreaField('a) If yes, when?')
    migratedwith = RadioField('b) If yes, did you move with your family or did you move alone?',
        choices=[('family', 'family'),
            ('alone', 'alone')], validators=[Optional()])
    migratedwithother = TextAreaField('other, specify')
    migratedwhy = RadioField('c) Why did you move?',
        choices=[('marriage', 'marriage'),
            ('economic migration', 'economic migration')], validators=[Optional()])
    migratedwhyother = TextAreaField('other, specify')
    houseownership = RadioField('16. What is the ownership of this house? Owned by',
        choices=[('respondent/spouse', 'respondent/spouse'),
            ('relative', 'relative'),
            ('rented or leased', 'rented or leased'),
            ('church-owned', 'church-owned')], validators=[Optional()])
    houseownershipother = TextAreaField('other, specify')
    housetype = RadioField('17.   What type of house is this?',
        choices=[('permanent', 'permanent (wall, floor and roof made of permanent (durable materials)'),
            ('semi-permanent', 'semi-permanent (combination of durable and bush materials)'),
            ('impoverished', 'impoverished (wall, floor and roof, all three made of bush and inferior quality materials)'),
            ('tent', 'tent')], validators=[Optional()])
    housetypeother = TextAreaField('other, specify')
    houselatrine = RadioField('18.  Does your household have a latrine?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    houselatrinekind = TextAreaField('If yes, what kind of latrine?')
    houselatrinenolatrine = RadioField('If no, what facility does your HH members use?',
        choices=[('neighbour toilet', 'neighbour toilet'),
            ('nearby bush', 'nearby bush'),
            ('river', 'river')], validators=[Optional()])
    houselatrinenolatrineother = TextAreaField('other, specify')
    hhrelativeclassification = RadioField('19. How do you classify your HH in relation to others in this village?',
        choices=[('very poor', 'very poor'),
            ('poor', 'poor'),
            ('average', 'average'),
            ('not poort', 'not poort'),
            ('rich', 'rich')], validators=[Optional()])
    hhrelativeclassificationreason = TextAreaField('Why do you classify so? Give reasons')
    spousesdiscussmoney = RadioField('20. Do you and your spouse discuss the distribution of money or financial matters together?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    spousesdiscussmoneyother = TextAreaField('other, specify')
    drinksalcohol = RadioField('21.  Do you drink any alcohol?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    drinksalcoholaffectsbehaviour = RadioField('a)   Do you ever drink to a point that alcohol affects your behaviour?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    drinksalcoholaffectsbehaviourfrequency = RadioField('a.  If Yes, how often this happens?',
        choices=[('once a week', 'once a week'),
            ('2 times a month', '2 times a month'),
            ('once a month', 'once a month')], validators=[Optional()])
    drinksalcoholaffectsbehaviourfrequencyother = TextAreaField('other, specify')
    drinksalcoholaggressive = RadioField('b)   Do you behave aggressively when you have been drinking alcohol?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    drinksalcoholdamagesorbeatings = RadioField('c)   Have you damaged your house or beaten your partner after you have been drinking?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    drinksalcoholregretsafter = RadioField('d)   Do you ever regret your behaviour after you have been drinking?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])

    gotoaidpost = RadioField('Do you go to aid post?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    gotoaidpostrank = IntegerField('rank', validators=[Optional()])
    gotohealthcenre = RadioField('Do you go to health centre?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    gotohealthcenrerank = IntegerField('rank', validators=[Optional()])
    gotohospital = RadioField('Do you go to hospital?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    gotohospitalrank = IntegerField('rank', validators=[Optional()])
    gototraditionalhealers = RadioField('Do you go to traditional healers?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    gototraditionalhealersrank = IntegerField('rank', validators=[Optional()])
    gotootherhealthservices = RadioField('Do you go to other health services?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    gotootherhealthservicesrank = IntegerField('rank', validators=[Optional()])
    gotohospitalreason = RadioField('23.  Do you go to the health services indicated above for?',
        choices=[('injuries', 'injuries'),
            ('family planning', 'family planning'),
            ('only in emergency situations', 'only in emergency situations'),
            ('never', 'never')], validators=[Optional()])
    gotohospitalreasonother = TextAreaField('other, specify')
    heardofsv = RadioField('24.   Have you heard of family and sexual violence?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    heardofsvsource = RadioField('If yes, from what sources?',
        choices=[('radio', 'radio'),
            ('newspapers', 'newspapers'),
            ('awareness by NGOs', 'awareness by NGOs'),
            ('friends and relatives', 'friends and relatives')], validators=[Optional()])
    fsvunderstanding = TextAreaField('25.   How do you understand FSV? According to your perception what does it include? (PROBE: physical abuse, sexual abuse, or mental abuse to the victim or other person at risk; or otherwise harms or endangers the health, safety or well-being of the victim or other person at risk)')
    awarenessofthelaw = RadioField('26.  Are you aware that domestic violence is against the law?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    awarenessofinterimprotectionorders = TextAreaField('27.   Are you aware of Interim Protection Orders? If yes, how does it make you understand the issue of domestic violence?')
    communitysvdiscussions = RadioField('28.  In your community, do you openly discuss issues of family and sexual violence? ',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    communitysvdiscussionsifno = TextAreaField('If no, what would be the reasons, please specify')
    alcoholanddruguseescalation = RadioField('29.  Has there been an escalation of alcohol use and drugs in your community during last 12 months? ',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    influxofmoney = RadioField('30.  In your opinion, is there an influx of money into the area?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    influxofmoneyreasons = TextAreaField('If yes, what are the reasons for the influx of money?')
    influxofmoneyescalatesdrugsandviolence = RadioField('Is the influx of money into this area escalating the use of alcohol and drugs, and increasing violence against women and girls, men and boys?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    influxofmoneyescalatesdrugsandviolencedetails = TextAreaField('please specify')
    formsofviolenceincommunity = TextAreaField('31. In the past 12 months, what forms of violence and abusive behaviors have you witnessed in your community by both men and women?')
    formsoffsvincommunity = TextAreaField('32. In the past 12 months, what are other forms of FSV in your community that are witnessed?')
    maincausesoffsv = TextAreaField('33.    What do you think/believe are the main causes of FSV?')
    changeincommunityattitude = RadioField('34.   Have you seen changes in community attitude towards violence experienced by women or men in the last 12 months? ',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    changeincommunityattitudedetails = TextAreaField('If yes, how has it changed?')
    goodwifeobeys = RadioField('35.   A good wife obeys her husband even if she disagrees. ',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    familyproblemsdiscussedinternally = RadioField('36. Family problems should only be discussed with people in the family. ',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    importanttoshowspousewhosboss = RadioField('37.   It is important for a man to show his wife/partner who is the boss. ',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    womanshouldchooseownfriends = RadioField('38.    A woman should be able to choose her own friends even if her husband disapproves.',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    wifeobligedtohavesex = RadioField('39.   It\'s a wife\'s obligation to have sex with her husband even if she doesn\'t feel like it.',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    outsidershouldintervene = RadioField('40.   If a man mistreats his wife, others outside of the family should intervene. ',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    sometimeswomandeservesbeatings = RadioField('41.   There are times when a woman deserves to be beaten. ',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    womanshouldtolerateviolence = RadioField('42.   A woman should tolerate violence in order to keep her family together. ',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    womanrefuseschildrenshouldbebeaten = RadioField('43.    If a woman does not want to have a child she deserves to be beaten?',
        choices=[('agree', 'agree'),
            ('disagree', 'disagree'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    womenshouldputwithabuse = RadioField('44.  How do you feel about the following statement: Women should put up with abuse in order to keep the family together?',
        choices=[('strongly agree', 'strongly agree'),
            ('agree', 'agree'),
            ("don't know", "don't know"),
            ('disagree', 'disagree'),
            ('strongly disagree', 'strongly disagree')], validators=[Optional()])
    womenhaverighttolitigate = TextAreaField('45.   Do women have rights to take a man who hits her to court?')
    bridepricemeantotalcontrol = TextAreaField('46.   Does bride price give a man total control over the woman?')
    domesticviolencelawawareness = TextAreaField('47.   Are you aware that wife beating is against the law?')
    denyingmoney = RadioField('a) Denying money of income',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    criticising = RadioField('b) Criticizing to make, feel bad or useless',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    preventingoutsidecontact = RadioField('c) Preventing contact with family and friends',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    yellingabuse = RadioField('d) Yelling abuse',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    threateninghurtingsomeone = RadioField('e) Threatening to hurt you or someone you care about',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    smashingobjects = RadioField('f) Throwing or smashing objects near the person',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    forcingtohavesex = RadioField('g) Forcing to have sex',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    slapping = RadioField('h) Slapping or pushing to harm or cause fear',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    hitting = RadioField('i) Hitting or punching',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    chokingorbeating = RadioField('j) Chocking or beating',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    stalking = RadioField('k) Stalking',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    harassment = RadioField('l) Harassment',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])
    intimidation = RadioField('m) Intimidation in some way',
        choices=[('1', '1'),
            ('2', '2'),
            ('3', '3'),
            ('4', '4')], validators=[Optional()])

# 49. Has your current husband / partner, or any other non-intimate partner ever…
    partnerinsulted = RadioField('Insulted you or made you feel bad about yourself?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerinsultedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerinsultedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerinsultedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerbelittled = RadioField('Belittled or humiliated you in front of other people?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerbelittledpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerbelittledpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerbelittledearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerintimidated = RadioField('Done things to scare or intimidate you on purpose (e.g. by the way he looked at you, by yelling and smashing things)?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerintimidatedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerintimidatedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerintimidatedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerthreatened = RadioField('Threatened to hurt you, destroy property or someone you care about?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerthreatenedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerthreatenedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerthreatenedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerstalked = RadioField('Stalked you?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerstalkedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerstalkedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerstalkedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    # 50. Has your current husband / partner, or any other non-intimate partner ever…
    partnerslapped = RadioField('Slapped you or thrown something at you that could hurt you?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerslappedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerslappedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerslappedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnershoved = RadioField('Pushed you or shoved you or pulled your hair?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnershovedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnershovedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnershovedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnerhit = RadioField('Hit you with his fist or with something else that could hurt you?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerhitpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerhitpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerhitearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnerkicked = RadioField('Kicked you, dragged you or beat you up?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerkickedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerkickedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerkickedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnerchocked = RadioField('Choked or burnt you on purpose?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerchockedpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerchockedpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerchockedearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnerthreatenedtouseaweapon = RadioField('Threatened to use or actually used a gun, knife or other weapon against you?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerthreatenedtouseaweaponpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerthreatenedtouseaweaponpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerthreatenedtouseaweaponearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnerforcedintercourse = RadioField('Did your current husband/partner or any other partner ever physically force you to have sex when you did not want to?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerforcedintercoursepastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerforcedintercoursepastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerforcedintercourseearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnerintercourseoutofintimidation = RadioField('Did you ever have sex you did not want to because you were afraid of what your partner or any other partner might do?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerintercourseoutofintimidationpastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerintercourseoutofintimidationpastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerintercourseoutofintimidationearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    partnerforcedsexualacts = RadioField('Did your partner or any other partner ever forced you to do something sexual that you found degrading or humiliating?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerforcedsexualactspastyear = RadioField('B) Has this happened in the past 12 months? (If YES, ask C only, if NO, ask D only.)',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    partnerforcedsexualactspastyeartimes = RadioField('C) In the past 12 months, how many times did it happen? (after asking this, move onto next item)',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])
    partnerforcedsexualactsearliertimes = RadioField('D) Before the past 12 months, how many times did it happen?',
        choices=[('once', 'once'),
            ('a few times', 'a few times'),
            ('many times', 'many times')], validators=[Optional()])

    hasbeenvictimoffsv = RadioField('51.  Have you been a victim of FSV by somebody else (who is a family member), by your own partner or both?',
        choices=[('partner', 'partner'),
            ('family member', 'family member'),
            ('both', 'both'),
            ('hasn\'t been a victim of FSV', 'hasn\'t been a victim of FSV')], validators=[Optional()])
    relationshiptoperpetrators = TextAreaField('Please, could you specify your relationship to the perpetrator(s)?')
    beenpregnant = RadioField('52.  Have you ever been pregnant?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    numberofpregnancies = IntegerField('Number of pregnancies', validators=[Optional()])
    hitduringpregnancy = RadioField('b) When you were pregnant, was there ever a time when you were slapped, hit or beaten by (any of) your partner(s)?',
        choices=[('yes', 'yes'),
            ('no', 'no'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    knowsothervictims = RadioField('53.   Would you know others in your village who are suffering from FSV?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    beeninjured = RadioField('54.  Have you ever been injured as a result of these acts by (any of) your husband / partner(s). Please think of the acts that we talked about before.',
        choices=[('yes', 'yes'),
            ('no', 'no'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    numberofinjuriesin12months = RadioField('55.  Over the last 12 months, how many times were you injured by (any of) your husband/partner(s)? Would you say once or twice, several times or many times?',
        choices=[('none', 'none'),
            ('once/twice', 'once/twice'),
            ('several (3-5) times', 'several (3-5) times'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    beeninjuredbyfamilymember = RadioField('56.  Have you ever been injured as a result of these acts by somebody who was not your partner but your family member?',
        choices=[('yes', 'yes'),
            ('no', 'no'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    numberofinjuriesbyfamilymemberin12months = RadioField('57.   Over the last 12 months, how many times were you injured by somebody who was not your partner but your family member? Would you say once or twice, several times or many times?',
        choices=[('none', 'none'),
            ('once/twice', 'once/twice'),
            ('several (3-5) times', 'several (3-5) times'),
            ("don't know", "don't know"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    drugrelatedabusein12months = RadioField('58.  In the last 12 months did you encounter any problem related to drug abuse?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    drugrelatedabusein12monthsperpetrator = RadioField('By who?',
        choices=[('spouse', 'spouse'),
            ('relatives', 'relatives')], validators=[Optional()])
    drugrelatedabusein12monthsperpetratorother = TextAreaField('other, specify')
    didfightback = RadioField('59. During the times that you were hit, did you ever fight back physically or to defend yourself? How often?',
        choices=[('never', 'never'),
            ('once or twice', 'once or twice'),
            ('several times', 'several times'),
            ('many times/most of the time', 'many times/most of the time'),
            ("don't know/remember", "don't know/remember"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    hithusbandinretaliation = RadioField('60. Have you ever hit or physically mistreated your husband/partner when he was mistreating you (retaliation / self-defense)? If yes, how often?',
        choices=[('never', 'never'),
            ('once or twice', 'once or twice'),
            ('several times', 'several times'),
            ('many times/most of the time', 'many times/most of the time'),
            ("don't know/remember", "don't know/remember"),
            ('refused/no answer', 'refused/no answer')], validators=[Optional()])
    perpetrators = TextAreaField('61.   Could you tell me, who are the perpetrators (e.g. someone in the family, a neighbour, someone at work, a friend)?')
    placeofviolence = RadioField('62. Where does the violence take place?',
        choices=[('at home', 'at home'),
            ('on the street', 'on the street'),
            ('at work', 'at work')], validators=[Optional()])
    placeofviolenceother = TextAreaField('other, specify')
    turntowhenvictimised = TextAreaField('63.   To whom do women turn after they have been victimized?')
    attackedandvisitedhealthfacility = TextAreaField('64.    Have you ever been attacked by your family member or other people and visited the health facility in your area for medical treatment?')
    othervictimsvisitedhealthcentre = RadioField('65.  Do you know, if other victims in your community visited the health centre for treatment?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    whatservicesareimportant = TextAreaField('66.    What services you think are important for somebody who has been a victim of FSV?')
    svservicesrating = RadioField('67. How would you rate the services that you see available or receive/ever received in case of sexual violation?',
        choices=[('excellent', 'excellent'),
            ('good', 'good'),
            ('fair', 'fair'),
            ('poor', 'poor')], validators=[Optional()])
    victimchallenges = TextAreaField('68.   What are some of the challenges you (would) experience when you have been victim of FSV?')
    overcommingchallenges = TextAreaField('69.   How has been/is the practice of overcoming the challenges mentioned above?')
    desiredsupport = RadioField('70. What kind of support would you like to receive?',
        choices=[('Legal aid', 'Legal aid'),
            ('Health services', 'Health services'),
            ('Counselling', 'Counselling'),
            ('Accommodation', 'Accommodation'),
            ('Police protection', 'Police protection'),
            ('Transport', 'Transport'),
            ('Capacity building - awareness raining', 'Capacity building - awareness raining')], validators=[Optional()])
    desiredsupportother = TextAreaField('other, specify')
    whatshouldbedonetopreventfsv = TextAreaField('71.   In your own opinion, what should be done about to prevent the prevalence of FSV among family members?')
    feelingafterinterview = RadioField('I have asked about many difficult things. How has talking about these things made you feel?',
        choices=[('good/better', 'good/better'),
            ('bad/worse', 'bad/worse'),
            ('same/no difference', 'same/no difference')], validators=[Optional()])

    comments = TextAreaField('Write down below any comments you have.')
    confirmation = BooleanField('Tick this box when you are ready to submit the form', validators=[Required()])

    submit = SubmitField('Submit')
