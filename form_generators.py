# from wtforms import StringField, SubmitField, BooleanField, TextAreaField
# from wtforms import DateField, RadioField, IntegerField, SelectField, FieldList
# from wtforms import SelectMultipleField, FloatField
# from wtforms.validators import Required, Length, NumberRange, Optional

# date = strftime("%Y-%m-%d %H:%M:%S", gmtime())
# villagepopulation = IntegerField('Village Population')
# numberofwomen = IntegerField('Number of Women')
# numberofmen = IntegerField('Number of Men')
# numberofhouseholds = IntegerField('Number of Households')
# dateofprofile = DateField('Date of Village Profile (e.g., 2016-01-31)')

class_name = 'PMVForm'
form_file = '/Users/zizz/Downloads/' + class_name + '.py'
html_file = '/Users/zizz/Downloads/tmp-2.txt'
init_text = """
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, BooleanField, TextAreaField
from wtforms import DateField, RadioField, IntegerField, SelectField, FieldList
from wtforms import SelectMultipleField, FloatField
from wtforms.validators import Required, Length, NumberRange, Optional
import json
from time import gmtime, strftime, time
""".strip() # because trailing spaces are annoying
init_text = init_text + "\n\nclass {}(Form):\n".format(class_name)
init_text = init_text + "    def __init__(self, id):\n"
init_text = init_text + "        super({}, self).__init__()\n".format(class_name)
init_text = init_text + "        self.id = id.format('form1')\n\n"

def init_form_class():
    with open(form_file, 'w') as file:
        file.write(init_text)

def make_section(text):
    with open(html_file, 'a') as file:
        file.write('<h3>{}</h3>\n'.format(text))

def make_paragraph(text):
    with open(html_file, 'a') as file:
        file.write('<p><strong>{}</strong></p>\n'.format(text))

def append_to_form(text):
    with open(form_file, 'a') as file:
        file.write('\t' + text)

def finalise_form_class():
    append_to_form("""
    comments = TextAreaField('Write down below any comments you have.')
    confirmation = BooleanField('Tick this box when you are ready to submit the form',
        validators=[Required()])

    submit = SubmitField('Submit')""")

def make_stringfield(text, name='', validators=''):
    if not name:
        name = ''.join(text.strip().lower().split())
    if validators:
        validators = ', validators[' + validators + ']'

    string = name + " = StringField('" + text + "'" + validators + ')\n'

    append_to_form(string)

    with open(html_file, 'a') as file:
        file.write('{{ renderfield(form.' + name + ') }}\n')

    # TODO: remove non-alphanumeric characters

def make_integerfield(text, name='', validators='[Optional()]'):
    if not name:
        name = ''.join(text.strip().lower().split())
    if validators:
        validators = ', validators[' + validators + ']'

    string = name + " = IntegerField('" + text + "'" + validators + ')\n'

    append_to_form(string)

    with open(html_file, 'a') as file:
        file.write('{{ renderfield(form.' + name + ') }}\n')

    # TODO: remove non-alphanumeric characters

def make_datefield(text, name='', validators=''):
    if not name:
        name = ''.join(text.strip().lower().split())
    if validators:
        validators = ', validators[' + validators + ']'

    string = name + " = DateField('" + text + "'" + validators + ')\n'

    append_to_form(string)

    with open(html_file, 'a') as file:
        file.write('{{ renderfield(form.' + name + ') }}\n')

def make_booleanfield(text, name='', validators=''):
    if not name:
        name = ''.join(text.strip().lower().split())
    if validators:
        validators = ', validators[' + validators + ']'

    string = name + " = BooleanField('" + text + "'" + validators + ')\n'

    append_to_form(string)

    with open(html_file, 'a') as file:
        file.write('{{ renderfield(form.' + name + ') }}\n')


def make_textareafield(text, name='', validators=''):
    if not name:
        name = ''.join(text.strip().lower().split())
    if validators:
        validators = ', validators[' + validators + ']'

    string = name + " = TextAreaField('" + text + "'" + validators + ')\n'

    append_to_form(string)

    with open(html_file, 'a') as file:
        file.write('{{ renderfield(form.' + name + ') }}\n')


# Testing
init_form_class()
make_stringfield('Village Name')
make_section('Questionnaire for PMV Operators')
make_section('We represent the Socio-Economic Impact Study for Reconstruction of Ramu Highway. We would like your assistance in providing us with information about road transports.')
make_datefield('Date of the Interview (e.g., 2016-01-31)', 'dateofinterview')
make_stringfield('Name of PMV Operator', validators='Length(3, 100)')
make_integerfield('Phone Number of PMV Operator', validators='Length(3, 100)')
append_to_form('''interviewer = RadioField('Interviewer',
        choices=[('Joshua', 'Joshua'),
            ('Kolis', 'Kolis'),
            ('Martin', 'Martin'),
            ('Michael', 'Michael'),
            ('Stella', 'Stella'),
            ('Heather', 'Heather')])\n''')
make_stringfield('Road Corridor')
make_stringfield('Name of Village')
make_paragraph('1. Which is the road you most frequently use?')
make_stringfield('Origin')
make_stringfield('Destination')
make_integerfield('Total length of the road you operate your PMV (km)', name='totallength')
make_integerfield('2. How many round-trips do you make on this road on average per week?', name='averageweeklytrips')
make_paragraph('3. What is the travel frequency (round-trips during one month) in:')
make_integerfield('dry months')
make_integerfield('wet months')
make_paragraph('4. What is the travel time in project (in hours, one-way trip) road during')
make_integerfield('dry season')
make_integerfield('wet season')
append_to_form('''pmvstopinrainyseason = RadioField('5. Does PMV service stop in the rainy season?',
        choices=[('no', 'No'), ('yes', 'Yes')])''')
make_integerfield('6. If yes, for how many months', name='pmvstopmonths')
make_textareafield('and why?', name='pmvstopwhy')
append_to_form('''respondenteducation = RadioField('7. What type of vehicle do you drive?',
        choices=[('car', 'a) Car'),
        ('15 seater bus', 'b) 15 seater bus'),
        ('25 seater bus', 'c) 25 seater bus'),
        ('truck', 'd) Truck (dyna)'),
        ('tracktor', 'e) Tracktor')], validators=[Optional()])''')
make_integerfield('8. If you are owner of the PMV, how many other PMVs do you own?')
append_to_form('''pmvleased = RadioField('9. Is your PMV leased?',
        choices=[('no', 'No'), ('yes', 'Yes')])''')
make_integerfield('if yes how much leasing fees do you pay per month?', name='pmvleasingfees')
append_to_form('''pmvrepairspayer = RadioField('If your PMV is leased, who usually pays for the vehicle repair?',
        choices=[('driver', 'Driver'), ('owner', 'Owner')])''')
make_integerfield('10. How much do you pay for repair on average per month?', name='repaircosts')
make_integerfield('11. How much do you pay per month for your tyre repairs?', name='tyrerepaircosts')
make_integerfield('12. What is the fuel cost/km?', name='fuelcosts')
append_to_form('''usualcargo = RadioField('13. What type of cargo do you usually carry?',
        choices=[('passengers', 'a) Passengers'),
        ('food', 'b) Food')], validators=[Optional()])''')
make_stringfield('nonfood, specify', name='usualcargoother')
make_integerfield('14. What is the average distance your vehicle travels km/day?', name='averagedistance')
make_integerfield('15. What is the minimum passenger fare you charge per kilometer?', name='minimumfare')
make_integerfield('16. What is the maximum passenger fare you charge per kilometer?', name='maximumfare')
make_integerfield('17. What is the freight fee you charge on your most frequent route per kilometer?', name='freightfee')
append_to_form('''setfares = RadioField('18. Are the passenger fares set?',
        choices=[('no', 'No'), ('yes', 'Yes')])''')
append_to_form('''faresetter = RadioField('If yes, who sets the price?',
        choices=[('owner', 'a) Owner'),
        ('organisation', 'b) Organisation')], validators=[Optional()])''')
make_stringfield('other, specify', name='faresetterother')
make_paragraph('19. How many PMVs operate on this project road now?')
make_integerfield('a) Car', name='carquantity')
make_integerfield('b) 15 seater bus', name='bus15seaterquantity')
make_integerfield('c) 25 seater bus', name='bus25seaterquantity')
make_integerfield('d) Light Truck', name='lighttruckquantity')
make_integerfield('e) Medium Truck', name='mediumtruckquantity')
make_integerfield('f) Heavy Truck', name='heavytruckquantity')
make_integerfield('g) Tractor', name='tractorquantity')
make_integerfield('20. How much do you earn per month by operating this vehicle?', name='pmvincome')
make_textareafield('21. Do you have problems of PMV operation due to present road conditions? If yes, please specify', name='pmvproblems')
make_textareafield('Other comments, notes', name='comments')


make_booleanfield('Land shortage')
make_textareafield('Generally do all families in the village have enough land to grow food crops and cash crops to sustain their livelihoods?',
    'enoughland')
finalise_form_class()
