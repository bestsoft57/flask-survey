from flask_wtf import Form
from wtforms import StringField, SubmitField, BooleanField, TextAreaField
from wtforms import DateField, RadioField, IntegerField, SelectField, FieldList
from wtforms import SelectMultipleField, HiddenField
from wtforms.validators import Required, Length, NumberRange, Optional
import json
from time import gmtime, strftime, time

class FinalTradeStoreForm(Form):
    def __init__(self, id):
        super(FinalTradeStoreForm, self).__init__()
        self.id = id

    # Basic Information
    start_time = HiddenField(default=str(int(time())))
    
    dateofinterview = DateField('Date of the interview (e.g., 2016-01-31)')
    namephoneofstorekeeper = StringField('Name and phone number of storekeeper/owner:', validators=[Length(3, 100)])
    interviewer = StringField('Name of the interviewer:')
    roadcorridor = StringField('Road Corridor')
    villagename = StringField('Name of Village and Province')

    tradestoresinfrontage = RadioField('1. Can you see any changes in moving goods along the road corridor prior to and after road rehabilitation?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    tradestoresinfrontageother = TextAreaField('If yes, please specify', render_kw={"rows": 2, "cols": 80})

    roadtomovegoodstotrade = StringField('2. Besides this road which is improved, is there any other road that you use to move your goods to your trade store (name of the road)?')
    roadforeasymovegoods = TextAreaField('3. If yes to 2, on which road it is easy to move goods and why?', render_kw={"rows": 2, "cols": 80})

    changeinnumberoftradestore = StringField('4. Is there any change in the number of trade stores that operate in this village, now and prior to road rehabilitation?')

    storesownbywomen = IntegerField('5. How many trade stores are owned by women in this village?', validators=[Optional()])
    ownorhirevehicle = RadioField('6. Do you own or hire vehicle(s) you use for moving your goods?',
        choices=[('Own', 'Own'),
            ('Hire', 'Hire')], validators=[Optional()])
    vehiclehirefee = IntegerField('7. If you hire vehicles, what is the hire fee?', validators=[Optional()])
    howmanytimeshire = IntegerField('8. How many times you hire in a month?', validators=[Optional()])

    changesinanswertoquestion = RadioField('9. Are there any changes in your answer to questions 7 and 8, before road upgrading and now?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    changesinanswertoquestionother = TextAreaField('If yes, please explain', render_kw={"rows": 2, "cols": 80})

    moveotherstuff = RadioField('10. Besides moving trade store goods, do you also move other stuff?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    moveotherstuffother = TextAreaField('If yes, please specify.', render_kw={"rows": 2, "cols": 80})

    anychangeincostoftransport = TextAreaField('11. Please explain to me if there is any change in cost of goods transport, frequency, travel time and place of purchase of such goods between now and prior to road rehabilitation', render_kw={"rows": 3, "cols": 80})

    averageincomeweeknow = IntegerField('', validators=[Optional()])
    averageincomemonthnow = IntegerField('', validators=[Optional()])
    averageincomechangeddetail = RadioField('Has this changed between now and prior to rehabilitation?', 
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    averageincomechangeddetailother=TextAreaField('If yes, please explain how much and reasons for changes?', render_kw={"rows": 2, "cols": 80})

    employeesofhavemale = IntegerField('(<input type="text" name="ageofmaleemployee"/>)', validators=[Optional()])
    employeesofhavefemale = IntegerField('(<input type="text" name="ageoffemaleemployee"/>)', validators=[Optional()])
    howmanychangedgenderofemployee = TextAreaField('Explain how many changed and gender of employees', render_kw={"rows": 2, "cols": 80})

    tradestorelocalproduced = RadioField('14. Does this trade store purchase locally produced materials for sale?', 
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    tradestorelocalproducedother = TextAreaField('If yes, what items and at what cost?', render_kw={"rows": 2, "cols": 80})
    estimatequantity = StringField("15. Can you estimate the quantity?")
    materialcollect = IntegerField('16. How often do you collect the materials?', validators=[Optional()])

    commoditiesresaleitems=RadioField('17. Does this trade store collect products from the village for outside traders?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])

    whatitemcollected = StringField('What items are collected?')
    traderoutside = IntegerField('How many traders (outside)?', validators=[Optional()])
    commdestimatequantity = StringField('Can you estimate the quantity?')
    whenproductscollected = StringField('When the products are collected?')

    questionsrehabilitationdetail=TextAreaField('Explain any change for questions 14 to 17 between now and prior to rehabilitation? What changes have occurred and give reasons:', render_kw={"rows": 5, "cols": 80})

    possessedgenerator = IntegerField('a) generator (type <input type="text" name="typeofgenerator" />)', validators=[Optional()])
    possessedfridge = IntegerField('b) fridge ', validators=[Optional()])
    possessedcooler = IntegerField('c) cooler', validators=[Optional()])
    possessedsolarunit = IntegerField('d) solar power unit', validators=[Optional()])
    possessedbus15seater = IntegerField('e) bus (15 seater)', validators=[Optional()])
    possessedbus25seater = IntegerField('f) bus (25  seater)', validators=[Optional()])
    possessedcar = IntegerField('g) car', validators=[Optional()])
    possessedmotorbike = IntegerField('h) motorbike', validators=[Optional()])
    possessedlighttruck = IntegerField('i) light truck', validators=[Optional()])
    possessedheavytruck = IntegerField('j) heavy truck', validators=[Optional()])
    possessedtractortrailer = IntegerField('k) tractor-trailer', validators=[Optional()])
    possessedtractor = IntegerField('l) tractor', validators=[Optional()])

    changeitesmdetail = TextAreaField('Has any of above items changed between now and prior to road project? Explain', render_kw={"rows": 2, "cols": 80})

    bushmaterialnow = StringField('', validators=[Optional()])
    bushmaterialprior = StringField('', validators=[Optional()])
    bushmaterialcomment = TextAreaField('', validators=[Optional()])

    semipermanentnow = StringField('', validators=[Optional()])
    semipermanentprior = StringField('', validators=[Optional()])
    semipermanentcomment = TextAreaField('', validators=[Optional()])

    permanentnow = StringField('', validators=[Optional()])
    permanentprior = StringField('', validators=[Optional()])
    permanentcomment = TextAreaField('', validators=[Optional()])

    numberofroomsnow = IntegerField('', validators=[Optional()])
    numberofroomsprior = IntegerField('', validators=[Optional()])
    numberofroomscomment = TextAreaField('', validators=[Optional()])

    storageareanow = IntegerField('', validators=[Optional()])
    storageareaprior = IntegerField('', validators=[Optional()])
    storageareacomment = TextAreaField('', validators=[Optional()])

    changestonumberofcustomer = RadioField('20. Are there any changes to number of customers and thefts between now and prior to road upgrading?', 
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    changestonumberofcustomerother = TextAreaField('If yes, please specify', render_kw={"rows": 2, "cols": 80})


    salttsprice=IntegerField('', validators=[Optional()])
    saltpcprice=IntegerField('', validators=[Optional()])

    noodlestsprice=IntegerField('', validators=[Optional()])
    noodlespcprice=IntegerField('', validators=[Optional()])

    sugar1kgtsprice=IntegerField('', validators=[Optional()])
    sugar1kgpcprice=IntegerField('', validators=[Optional()])

    suger500gtsprice=IntegerField('', validators=[Optional()])
    suger500gpcprice=IntegerField('', validators=[Optional()])

    rice1kgtsprice=IntegerField('', validators=[Optional()])
    rice1kgpcprice=IntegerField('', validators=[Optional()])

    rice500gtsprice=IntegerField('', validators=[Optional()])
    rice500gpcprice=IntegerField('', validators=[Optional()])

    soapbartsprice=IntegerField('', validators=[Optional()])
    soapbarpcprice=IntegerField('', validators=[Optional()])

    petroltsprice=IntegerField('', validators=[Optional()])
    petrolpcprice=IntegerField('', validators=[Optional()])

    fishbigtsprice=IntegerField('', validators=[Optional()])
    fishbigpcprice=IntegerField('', validators=[Optional()])

    fishsmalltsprice=IntegerField('', validators=[Optional()])
    fishsmallpcprice=IntegerField('', validators=[Optional()])

    meatbigtsprice=IntegerField('', validators=[Optional()])
    meatbigpcprice=IntegerField('', validators=[Optional()])

    meatsmalltsprice=IntegerField('', validators=[Optional()])
    meatsmallpcprice=IntegerField('', validators=[Optional()])

    kerosenetsprice=IntegerField('', validators=[Optional()])
    kerosenepcprice=IntegerField('', validators=[Optional()])

    bottlewatertsprice=IntegerField('', validators=[Optional()])
    bottlewaterpcprice=IntegerField('', validators=[Optional()])

    oiltsprice=IntegerField('', validators=[Optional()])
    oilpcprice=IntegerField('', validators=[Optional()])

    saltpriceapplicable=BooleanField('Packet of salt')
    noodlespriceapplicable=BooleanField('Noodles')
    sugar1kgpriceapplicable=BooleanField('Sugar 1kg')
    suger500gpriceapplicable=BooleanField('Sugar 500 g')
    rice1kgpriceapplicable=BooleanField('Rice 1kg')
    rice500gpriceapplicable=BooleanField('Rice 500 g')
    soapbarpriceapplicable=BooleanField('Soap bar (loose)')
    petrolpriceapplicable=BooleanField('Petrol liter')
    fishbigpriceapplicable=BooleanField('Tinned fish (big)')
    fishsmallpriceapplicable=BooleanField('Tinned fish (small)')
    meatbigpriceapplicable=BooleanField('Tinned meat Ox & Palm (big)')
    meatsmallpriceapplicable=BooleanField('Tinned meat Ox & Palm (small)')
    kerosenepriceapplicable=BooleanField('Kerosene')

    tradestoreoperatordetail = TextAreaField('Interviewer: <u><i>Please highlight the main changes between baseline and now!</i></u>', render_kw={"rows": 2, "cols": 80})
    tradestoresummarizedetail = TextAreaField('Interviewer: <u><i>Please summarize main changes between baseline and now. Please write a report.</i></u>', render_kw={"rows": 2, "cols": 80})

    # Final Steps
    comments = TextAreaField('Write down below any comments you have.', render_kw={"rows": 2, "cols": 80})
    confirmation = BooleanField('Confirm that you have finished entering all the information before submitting',
        validators=[Required()])

    submit = SubmitField('Submit')
