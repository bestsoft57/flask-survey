from flask_wtf import Form
from wtforms import StringField, SubmitField, BooleanField, TextAreaField
from wtforms import DateField, RadioField, IntegerField, SelectField, FieldList
from wtforms import SelectMultipleField, HiddenField
from wtforms.validators import Required, Length, NumberRange, Optional
import json
from time import gmtime, strftime, time

class HouseholdForm(Form):
    def __init__(self, id):
        super(HouseholdForm, self).__init__()
        self.id = id

    # Basic Information
    start_time = HiddenField(default=str(int(time())))
    province = StringField('PROVINCE')
    district = StringField('DISTRICT and LLG Name')
    ward = StringField('WARD Name/Number')
    village = StringField('VILLAGE Name')
    road = StringField('Road name')
    householdnumber = IntegerField('Household number (given by study team)', validators=[Optional()])
    headofhouseholdname = StringField('Name of the household head')
    headofhouseholdsex = StringField('Sex of the household head')
    intervieweename = StringField('Name of the person interviewed and relation to the head of the household')
    clansubclan = StringField('Clan and sub-clan name')
    researchassistant = StringField('Name of the Research Assistant')
    interviewdate = DateField('Date of the interview (e.g., 2016-01-31)')
    samepeoplenumber = IntegerField('Number of people living in the same household with you', validators=[Optional()])
    
    affectedperson = RadioField('Please circle whether the person is Affected Person:',
       choices=[('yes', 'yes'),
       ('semi-no', 'no')], validators=[Optional()])
    affectedspecifics = RadioField('If an AP, what did you loose due to this road expansion?',
        choices=[('house', 'a) House'),
            ('trees and/or crops', 'b) Trees and/or crops'),
            ('other structures', 'c) Other structures'),
            ('land', 'd) Land'),
            ('cemetery', 'e) Cemetery'),
            ('other', 'f) Other')],
        validators=[Optional()])

    # Household Characteristics
    respondentgender = RadioField('1. Gender of the respondent',
        choices=[('male', 'Male'), ('female', 'Female')], validators=[Optional()])
    huswifepresent = RadioField('2. Is the husband or wife present during interview?',
        choices=[('Yes', 'Yes'), ('No', 'No'), ('Single-headed hh', 'Single-headed hh')], validators=[Optional()])
    respondentage = IntegerField('3. Age of the respondent', validators=[Optional()])

    hhheadliteracy = RadioField('4. HH head’s literacy:',
        choices=[('local dialiect', 'local dialiect'),
            ('English', 'English'),
            ('Pidgin', 'Pidgin')], validators=[Optional()])
    spouseliteracy = RadioField('5. Spouse literacy:',
        choices=[('local dialiect', 'local dialiect'),
            ('English', 'English'),
            ('Pidgin', 'Pidgin')], validators=[Optional()]) 

    respondentmainjob = RadioField('6. Main job of the respondent:',
        choices=[('farmer', 'a) subsistence farmer'), ('vendor', 'b) store vendor'), 
        ('teacher', 'c) teacher'), ('no job', 'd) disabled (no job)'),
        ('private sector employee', 'e) private sector employee'),
        ('other', 'f) other')], 
        validators=[Optional()])
    respondentmainjobother = TextAreaField('Other, specify')
    spousemainjob = RadioField('7. Spouse\'s main job: ',
        choices=[('farmer', 'a) subsistence farmer'), ('vendor', 'b) store vendor'), 
        ('teacher', 'c) teacher'), ('other', 'd) other')], 
        validators=[Optional()])
    spousemainjobother = TextAreaField('Other, specify')
    respondenteducation = RadioField('8. Education level of the respondent:',
        choices=[('none', 'a) never went to school'),
        ('primary', 'b) primary education (grades 1-8)'),
        ('secondaryschool', 'c) secondary school education (grade 9 to 12)'),
        ('college', 'd) college educational qualification'),
        ('university', 'e) university educational qualification'),
        ('other', 'f) other')], validators=[Optional()])
    spouseeducation = RadioField('9. Education level of spouse:',
        choices=[('none', 'a) never went to school'),
        ('primary', 'b) primary education (grades 1-8)'),
        ('secondaryschool', 'c) secondary school education (grade 9 to 12)'),
        ('college', 'd) college educational qualification'),
        ('university', 'e) university educational qualification'),
        ('other', 'f) other')], validators=[Optional()])
    migratedatetolocation = RadioField('10. Did you migrate to your present location from somewhere?',
        choices=[('Yes', 'Yes'), ('No', 'No')], validators=[Optional()])
    migratewherewhen = StringField('If Yes, from where and when?')
    numberofchildren = IntegerField('11. Number of children (include all living either with parents or separately):', validators=[Optional()])
    numberofboys = IntegerField('12. Number of boys (include all living, include married):', validators=[Optional()])
    numberofgirls = IntegerField('13. Number of girls (include all living, include married):', validators=[Optional()])
    howfarhouse = RadioField('14. How far away from the project road is your current house located (distance in metres)?',
        choices=[('0m', 'a) 0 - 50m'), 
        ('50m', 'b) 50 – 200m'), 
        ('200m', 'c) 200 – 500m'),
        ('500m', 'd) 500m or more')], 
        validators=[Optional()])
    howfarawayproject = IntegerField('15. If your village is a control village, state how far is your house away from the project road', validators=[Optional()])
    howfarawaycontrol = IntegerField('(km) and control road', validators=[Optional()])

    # Household physical features and assets
    houseownership = RadioField('16. What is the ownership of this house? Owned by',
        choices=[('repondent/spouse', 'a) the respondent/spouse'),
        ('relative', 'b) relative'), ('rented or leased', 'c) rented or leased'),
        ('church owned', 'd) church owned'), ('other', 'e) other')], validators=[Optional()])
    houseownershipother = TextAreaField('Other, specify')
    landownership = RadioField('17. Who owns the land this house is built on? Owned by',
        choices=[('respondent/spouse', 'a) the respondent/spouse'),
        ('village', 'b) traditional (i.e. village land)'),
        ('government', 'c) Government (i.e. alienated land)'),
        ('church', 'd) church owned'),
        ('family', 'e) private land owned by family'),
        ('dispute', 'f) land in dispute'),
        ('other', 'g) other')], validators=[Optional()])
    landownershipother = TextAreaField('Other, specify')
    typeofhouse = RadioField('18. What type of house is this?',
       choices=[('permanent', 'a) permanent (wall, floor and roof made of permanent (durable materials))'),
       ('semi-permanent', 'b) semi-permanent (combination of durable and bush materials)'),
       ('impoverished', 'c) impoverished (wall, floor and roof, all three made of bush and inferior quality materials)'),
       ('tent', 'd) tent'), ('other', 'e) other')], validators=[Optional()])
    typeofhouseother = TextAreaField('Other, specify')
    houseelectricity = RadioField('19. Does this house have electricity?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    houseelectricitytype = RadioField('If yes, is it',
        choices=[('grid', 'a) grid'),
            ('private own generator', 'b) private own generator'),
            ('private shared generator', 'c) private shared generator'),
            ('solar', 'd) solar'), ('other', 'e) other')], validators=[Optional()])
    houseelectricitytypeother = TextAreaField('Other, specify')
    othersourceoflight = StringField('20. If house has no electricity, what is the main source of light?')


    # 21. What types of assets are currently owned by this HH?
    televisionbefore = IntegerField('', validators=[Optional()])
    VCRbefore = IntegerField('', validators=[Optional()])
    cassetteCDplayerbefore = IntegerField('', validators=[Optional()])
    camerabefore = IntegerField('', validators=[Optional()])
    radiobefore = IntegerField('', validators=[Optional()])
    pcbefore = IntegerField('', validators=[Optional()])
    mobilephonebefore = IntegerField('', validators=[Optional()])
    bicyclebefore = IntegerField('', validators=[Optional()])
    motorbikebefore = IntegerField('', validators=[Optional()])
    cartruckbusbefore = IntegerField('', validators=[Optional()])
    boatbefore = IntegerField('', validators=[Optional()])
    furniturebefore = IntegerField('', validators=[Optional()])

    # 22. Livestock owned by this HH
    cattlebefore = IntegerField('', validators=[Optional()])
    sheepbefore = IntegerField('', validators=[Optional()])
    pigsbefore = IntegerField('', validators=[Optional()])
    goatsbefore = IntegerField('', validators=[Optional()])
    poultrybefore = IntegerField('', validators=[Optional()])
    fishbefore = IntegerField('', validators=[Optional()])
    otherlibstackbefore = TextAreaField('', validators=[Optional()])

    # 23. Agriculture

    totalgardens = IntegerField('23. How many gardens do you have in total?', validators=[Optional()])
    cropsaibika = BooleanField('a) aibika')
    cropsbanana = BooleanField('b) banana')
    cropsbeans = BooleanField('c) beans')
    cropscabbage = BooleanField('d) cabbage')
    cropscassava = BooleanField('e) cassava')
    cropscucumber = BooleanField('f) cucumber')
    cropskaukau = BooleanField('g) kaukau')
    cropspeanut = BooleanField('h) peanut')
    cropspineapple = BooleanField('i) pineapple')
    cropspitpit = BooleanField('j) pitpit')
    cropspotato = BooleanField('k) potato')
    cropspumpkin = BooleanField('l) pumpkin')
    cropssingapore = BooleanField('m) Singapore')
    cropssugarcane = BooleanField('n) sugarcane')
    cropssweetpotato = BooleanField('o) sweet potato')
    cropstapioca = BooleanField('p) tapioca')
    cropstaro = BooleanField('q) taro')
    cropstobacco = BooleanField('r) tobacco')
    cropswatermelon = BooleanField('s) watermelon')
    cropsyam = BooleanField('t) yam')
    cropscoffee = BooleanField('u) coffee')
    cropsother = TextAreaField('Other, please specify')

    madehowmanygardens = IntegerField('25. How many new gardens have you made recently?', validators=[Optional()])
    shareforconsumption = IntegerField('Out of the new gardens, what is the share for own consumption?', validators=[Optional()])
    shareforgeneration = IntegerField('What is the share for cash generation?', validators=[Optional()])

    winthingardens = IntegerField('26. If you have made new gardens, how many are: 1) within 500 m ?', validators=[Optional()])
    outsidegardens = IntegerField('2) outside?', validators=[Optional()])
    gardenestablishdifficulty = TextAreaField('27. What are some of the difficulties you are facing in regard to establishing a new garden:')
    
    changestocrops = RadioField('21.  Have there been any changes to the type of food and cash crops you mainly cultivate since the road upgrading?',
        choices=[('yes', 'yes'),
            ('no', 'no')], validators=[Optional()])
    changestocropsdetails = TextAreaField('If yes, please specify')

    # Before
    villagemarketnamebefore = StringField('')
    villagemarketdistancebefore = IntegerField('',validators=[Optional()])
    villagemarkettravelmodebefore = RadioField('',choices=[('walking', 'walking'),('bus/PMV', 'bus/PMV'), ('car', 'car')],validators=[Optional()])
    villagemarketwalkingtimebefore = IntegerField('',validators=[Optional()])
    villagemarketpmvtimebefore = IntegerField('',validators=[Optional()])
    villagemarketpmvcostbefore = IntegerField('',validators=[Optional()])
    villagemarketfrequencybefore = IntegerField('',validators=[Optional()])
    villagemarketincomepertripbefore = IntegerField('',validators=[Optional()])
    villagemainsellbefore = StringField('')

    # Before
    townmarketnamebefore = StringField('')
    townmarketdistancebefore = IntegerField('',validators=[Optional()])
    townmarkettravelmodebefore = RadioField('',choices=[('walking', 'walking'),('bus/PMV', 'bus/PMV'), ('car', 'car')],validators=[Optional()])
    townmarketwalkingtimebefore = IntegerField('',validators=[Optional()])
    townmarketpmvtimebefore = IntegerField('',validators=[Optional()])
    townmarketpmvcostbefore = IntegerField('',
       validators=[Optional()])
    townmarketfrequencybefore = IntegerField('',validators=[Optional()])
    townmarketincomepertripbefore = IntegerField('',validators=[Optional()])
    townmainsellbefore = StringField('')

    sellanimals = RadioField('29. Do you sell animals?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    sellanimalsother = TextAreaField('if yes, specify')
    traderscometothisvillage = RadioField('30. Do traders come to this village and purchase food items, animals and cash crops?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    traderscometothisvillageyes = StringField('If yes, what months do they come')
    traderscometothisvillagehowoften = StringField('and how often?')
    satisfiedwithpricesoffered = RadioField('If yes, are you satisfied with the prices offered?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])

    #31. Do you sell different products now than prior to road construction? (list all crops, animals and other) Please, specify
    differentproductsprior = TextAreaField('', render_kw={"rows": 5, "cols": 80})

    #32. Explain reasons for change in products sold:
    reasonchange = TextAreaField('', render_kw={"rows": 3, "cols": 80})


    #33
    overallhealthstatusnow = RadioField('33. What is the overall health status of this HH?',
        choices=[('excellent', 'a) excellent'),
            ('good', 'b) good'),
            ('average', 'c) average'),
            ('critical', 'd) critical')],
        validators=[Optional()])

    # Health facilities
    aidpostlocation = StringField('')
    aidpostdistance = IntegerField('',
       validators=[Optional()])
    aidposttrasnportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('truck', '(3) truck'),
            ('car', '(4) car')],
        validators=[Optional()])
    aidpostcostofreturntripnow = IntegerField('',
       validators=[Optional()])
    aidposttraveltimenow = IntegerField('',
       validators=[Optional()])

    healthcentrelocation = StringField('')
    healthcentredistance = IntegerField('',
       validators=[Optional()])
    healthcentretrasnportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('truck', '(3) truck'),
            ('car', '(4) car')],
        validators=[Optional()])
    healthcentrecostofreturntripnow = IntegerField('',
       validators=[Optional()])
    healthcentretraveltimenow = IntegerField('',
       validators=[Optional()])

    traditionallocation = StringField('')
    traditionaldistance = IntegerField('',
       validators=[Optional()])
    traditionaltrasnportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    traditionalcostofreturntripnow = IntegerField('',
       validators=[Optional()])
    traditionaltraveltimenow = IntegerField('',
       validators=[Optional()])

    hospitallocation = StringField('')
    hospitaldistance = IntegerField('',
       validators=[Optional()])
    hospitaltrasnportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    hospitalcostofreturntripnow = IntegerField('',
       validators=[Optional()])
    hospitaltraveltimenow = IntegerField('',
       validators=[Optional()])

    mobilehealthteamlocation = StringField('')
    mobilehealthteamdistance = IntegerField('',
       validators=[Optional()])
    mobilehealthteamtrasnportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    mobilehealthteamcostofreturntripnow = IntegerField('',
       validators=[Optional()])
    mobilehealthteamtraveltimenow = IntegerField('',
       validators=[Optional()])

    otherfacilitylocation = StringField('')
    otherfacilitydistance = IntegerField('',
       validators=[Optional()])
    otherfacilitytrasnportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    otherfacilitycostofreturntripnow = IntegerField('',
       validators=[Optional()])
    otherfacilitytraveltimenow = IntegerField('',
       validators=[Optional()])

    biggestproblemsasthmanow = BooleanField('a) asthma')
    biggestproblemspneumonianow = BooleanField('b) pneumonia')
    biggestproblemscoughnow = BooleanField('c) cough')
    biggestproblemsflunow = BooleanField('d) flu')
    biggestproblemsfevernow = BooleanField('e) fever')
    biggestproblemsbodyachesnow = BooleanField('f) body aches')
    biggestproblemsheadachenow = BooleanField('g) head ache')
    biggestproblemstbnow = BooleanField('h) T/B')
    biggestproblemsmalarianow = BooleanField('i) malaria')
    biggestproblemsdiarrheanow = BooleanField('j) diarrea')
    biggestproblemssoresnow = BooleanField('k) sores')
    biggestproblemsnowother = TextAreaField('Other, explain?')

    #Family clinic
    fclinicaidpostadultrecent=IntegerField('',validators=[Optional()])
    fclinicaidpostchildrecent=IntegerField('',validators=[Optional()])
    
    fclinictraditionaladultrecent=IntegerField('',validators=[Optional()])
    fclinictraditionalchildrecent=IntegerField('',validators=[Optional()])

    fclinichealthcentreadultrecent=IntegerField('',validators=[Optional()])
    fclinichealthcentrechildrecent=IntegerField('',validators=[Optional()])

    fclinichospitaladultrecent=IntegerField('',validators=[Optional()])
    fclinichospitalchildrecent=IntegerField('',validators=[Optional()])

    fclinicmobilehealthteamadultrecent=IntegerField('',validators=[Optional()])
    fclinicmobilehealthteamchildrecent=IntegerField('',validators=[Optional()])

    fclinicvolunteeradultrecent=IntegerField('',validators=[Optional()])
    fclinicvolunteerchildrecent=IntegerField('',validators=[Optional()])

    # Sanitation
    hhtoilet = RadioField('37. Does your household have a toilet?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')],
        validators=[Optional()])
    hhkindoftoilet = RadioField('If yes, what kind of a toilet?',
        choices=[('flush_toilet', 'a) flush toilet'),
            ('ventilated_improved_pit_latrine', 'b) ventilated improved pit latrine'),
            ('pit_latrine_with_slab', 'c) pit latrine with slab'),
            ('pit_latrine_without_slab/open_pit', 'd) pit latrine without slab/open pit'),
            ('other', 'e) other')],
        validators=[Optional()])
    hhkindoftoiletother = TextAreaField('Other, Specify')

    hhmemberfacility = RadioField('If no, what facility does your HH members use?',
        choices=[('neighbour_toilet', 'a) neighbour toilet'),
            ('nearby_bush', 'b) nearby bush'),
            ('river', 'c) river'),
            ('other', 'd) other')],
        validators=[Optional()])
    hhmemberfacilityother = TextAreaField('Other, Specify')

    # Sanitation of HH Changed
    HHSanitationChanged = TextAreaField('36. Has the sanitation of this HH changed somehow after road construction? If yes, please describe?')

    # Location of schools

    elementaryschoolnumberofchildren = IntegerField('',
       validators=[Optional()])
    elementaryschoollocation = StringField('')
    elementaryschooltransportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    elementaryschooltraveltimenow = IntegerField('',
       validators=[Optional()])
    elementaryschooltripcostnow = IntegerField('',
       validators=[Optional()])
    elementaryschooldistance = IntegerField('',
       validators=[Optional()])

    lowerprimaryschoolnumberofchildren = IntegerField('',
       validators=[Optional()])
    lowerprimaryschoollocation = StringField('')
    lowerprimaryschooltransportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    lowerprimaryschooltraveltimenow = IntegerField('',
       validators=[Optional()])
    lowerprimaryschooltripcostnow = IntegerField('',
       validators=[Optional()])
    lowerprimaryschooldistance = IntegerField('',
       validators=[Optional()])

    upperprimaryschoolnumberofchildren = IntegerField('',
       validators=[Optional()])
    upperprimaryschoollocation = StringField('')
    upperprimaryschooltransportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    upperprimaryschooltraveltimenow = IntegerField('',
       validators=[Optional()])
    upperprimaryschooltripcostnow = IntegerField('',
       validators=[Optional()])
    upperprimaryschooldistance = IntegerField('',
       validators=[Optional()])

    secondaryschoolnumberofchildren = IntegerField('',
       validators=[Optional()])
    secondaryschoollocation = StringField('')
    secondaryschooltransportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    secondaryschooltraveltimenow = IntegerField('',
       validators=[Optional()])
    secondaryschooltripcostnow = IntegerField('',
       validators=[Optional()])
    secondaryschooldistance = IntegerField('',
       validators=[Optional()])

    otherschoolnumberofchildren = IntegerField('',
       validators=[Optional()])
    otherschoollocation = StringField('')
    otherschooltransportnow = RadioField('',
        choices=[('walking', '(1) walking'),
            ('bus', '(2) bus'),
            ('PMV', '(3) PMV'),
            ('car', '(4) car')],
        validators=[Optional()])
    otherschooltraveltimenow = IntegerField('',
       validators=[Optional()])
    otherschooltripcostnow = IntegerField('',
       validators=[Optional()])
    otherschooldistance = IntegerField('',
       validators=[Optional()])

    schoolagednotinschoolchildrennow = RadioField('39. Do you have non-school children?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    schoolagednotinschoolboysnow = IntegerField('and boys?',
       validators=[Optional()])
    schoolagednotinschoolgirlsnow = IntegerField('If yes, how many girls?',
       validators=[Optional()])
    schoolagednotinschoolreasonnow = RadioField('40. If you have non-school children, what are the reasons for not going to school?',
        choices=[('School is too far', 'a) School is too far'),
        ('Road is not safe', 'b) Road is not safe'),
        ('No road', 'c) No road'),
        ('Fees are too high', 'd) Fees are too high'),
        ('Parents do not see that education is necessary', 'e) Parents do not see that education is necessary'),
        ('Children\'s work is needed at home', 'f) Children\'s work is needed at home'),
        ('Poor quality of education', 'g) Poor quality of education'),
        ('Teachers are absent', 'h) Teachers are absent'),
        ('Not safe for girls', 'i) Not safe for girls'),
        ('disable or sick', 'j) disable or sick'),
        ('other', 'k) other')],
        validators=[Optional()])
    schoolagednotinschoolreasonnowother=TextAreaField('Other reasons, explain?')

    dropoutsgirls = IntegerField('If yes, how many girls?', validators=[Optional()])
    dropoutsboys = IntegerField('and boys?', validators=[Optional()])

    dropoutsreasonsbefore = TextAreaField('42. If you have school-aged children who have dropped out school, what are the reasons', render_kw={"rows": 2, "cols": 80})


    # Household income
    transportincomepermonth = IntegerField('', validators=[Optional()])
    transportincontributemen = IntegerField('', validators=[Optional()])
    transportincontributewomen = IntegerField('', validators=[Optional()])
    transportincontributegirl = IntegerField('', validators=[Optional()])
    transportincontributeboy = IntegerField('', validators=[Optional()])

    pmvoperationincomepermonth = IntegerField('', validators=[Optional()])
    pmvoperationincontributemen = IntegerField('', validators=[Optional()])
    pmvoperationincontributewomen = IntegerField('', validators=[Optional()])
    pmvoperationincontributegirl = IntegerField('', validators=[Optional()])
    pmvoperationincontributeboy = IntegerField('', validators=[Optional()])

    tradestoreincomepermonth = IntegerField('', validators=[Optional()])
    tradestoreincontributemen = IntegerField('', validators=[Optional()])
    tradestoreincontributewomen = IntegerField('', validators=[Optional()])
    tradestoreincontributegirl = IntegerField('', validators=[Optional()])
    tradestoreincontributeboy = IntegerField('', validators=[Optional()])

    minimarketincomepermonth = IntegerField('', validators=[Optional()])
    minimarketincontributemen = IntegerField('', validators=[Optional()])
    minimarketincontributewomen = IntegerField('', validators=[Optional()])
    minimarketincontributegirl = IntegerField('', validators=[Optional()])
    minimarketincontributeboy = IntegerField('', validators=[Optional()])

    fuelsaleincomepermonth = IntegerField('', validators=[Optional()])
    fuelsaleincontributemen = IntegerField('', validators=[Optional()])
    fuelsaleincontributewomen = IntegerField('', validators=[Optional()])
    fuelsaleincontributegirl = IntegerField('', validators=[Optional()])
    fuelsaleincontributeboy = IntegerField('', validators=[Optional()])

    salesgardenfoodincomepermonth = IntegerField('', validators=[Optional()])
    salesgardenfoodincontributemen = IntegerField('', validators=[Optional()])
    salesgardenfoodincontributewomen = IntegerField('', validators=[Optional()])
    salesgardenfoodincontributegirl = IntegerField('', validators=[Optional()])
    salesgardenfoodincontributeboy = IntegerField('', validators=[Optional()])

    saleslivestockincomepermonth = IntegerField('', validators=[Optional()])
    saleslivestockincontributemen = IntegerField('', validators=[Optional()])
    saleslivestockincontributewomen = IntegerField('', validators=[Optional()])
    saleslivestockincontributegirl = IntegerField('', validators=[Optional()])
    saleslivestockincontributeboy = IntegerField('', validators=[Optional()])

    othertypeofbusiness = StringField('Other (specify)')
    otherincomepermonth = IntegerField('', validators=[Optional()])
    otherincontributemen = IntegerField('', validators=[Optional()])
    otherincontributewomen = IntegerField('', validators=[Optional()])
    otherincontributegirl = IntegerField('', validators=[Optional()])
    otherincontributeboy = IntegerField('', validators=[Optional()])

    
    earningsincomepermonth = IntegerField('', validators=[Optional()])
    earningsbymen = IntegerField('', validators=[Optional()])
    earningsbywomen = IntegerField('', validators=[Optional()])
    earningsbychildrengirl = IntegerField('', validators=[Optional()])
    earningsbychildrenboy = IntegerField('', validators=[Optional()])

    remittancesincomepermonth = IntegerField('', validators=[Optional()])
    remittancesbymen = IntegerField('', validators=[Optional()])
    remittancesbywomen = IntegerField('', validators=[Optional()])
    remittancesbychildrengirl = IntegerField('', validators=[Optional()])
    remittancesbychildrenboy = IntegerField('', validators=[Optional()])

    employmentincomepermonth = IntegerField('', validators=[Optional()])
    employmentbymen = IntegerField('', validators=[Optional()])
    employmentbywomen = IntegerField('', validators=[Optional()])
    employmentbychildrengirl = IntegerField('', validators=[Optional()])
    employmentbychildrenboy = IntegerField('', validators=[Optional()])

    royaltiesincomepermonth = IntegerField('', validators=[Optional()])
    royaltiesbymen = IntegerField('', validators=[Optional()])
    royaltiesbywomen = IntegerField('', validators=[Optional()])
    royaltiesbychildrengirl = IntegerField('', validators=[Optional()])
    royaltiesbychildrenboy = IntegerField('', validators=[Optional()])

    compensationsincomepermonth = IntegerField('', validators=[Optional()])
    compensationsbymen = IntegerField('', validators=[Optional()])
    compensationsbywomen = IntegerField('', validators=[Optional()])
    compensationsbychildrengirl = IntegerField('', validators=[Optional()])
    compensationsbychildrenboy = IntegerField('', validators=[Optional()])

    selfsuffientnow = RadioField('45. Is your household self-sufficient in regard to food stuffs locally produced?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    howdmanydaysfoodsupplies = RadioField('46. For how many days you currently have food supplies before you have to look for them again for your family?',
        choices=[('one day', 'a) one day'),
            ('one week', 'b) one week'),
            ('one month', 'c) one month'),
            ('six months', 'd) six months'),
            ('one year', 'e) one year'),
            ('other', 'f) other')], validators=[Optional()])
    howdmanydaysfoodsuppliesother = TextAreaField('Other, explain?')
    foodsupplyestimation = TextAreaField('What is the basis for your estimation?', render_kw={"rows": 2, "cols": 80})
    boughtfromoutsidehome = IntegerField('', validators=[Optional()])
    comefromhomeproduction = IntegerField('', validators=[Optional()])

    personalcareitemshowmuchpermonth = IntegerField('', validators=[Optional()])
    personalcareitemsperyear = IntegerField('', validators=[Optional()])

    housetoilethowmuchpermonth = IntegerField('', validators=[Optional()])
    housetoiletperyear = IntegerField('', validators=[Optional()])

    healthmedicinehowmuchpermonth = IntegerField('', validators=[Optional()])
    healthmedicineperyear = IntegerField('', validators=[Optional()])

    schoolfeehowmuchpermonth = IntegerField('', validators=[Optional()])
    schoolfeeperyear = IntegerField('', validators=[Optional()])

    phonecardhowmuchpermonth = IntegerField('', validators=[Optional()])
    phonecardperyear = IntegerField('', validators=[Optional()])

    maintenancehowmuchpermonth = IntegerField('', validators=[Optional()])
    maintenanceperyear = IntegerField('', validators=[Optional()])

    petroloilhowmuchpermonth = IntegerField('', validators=[Optional()])
    petroloilperyear = IntegerField('', validators=[Optional()])

    transportationhowmuchpermonth = IntegerField('', validators=[Optional()])
    transportationperyear = IntegerField('', validators=[Optional()])

    readymademanhowmuchpermonth = IntegerField('', validators=[Optional()])
    readymademanperyear = IntegerField('', validators=[Optional()])

    readymadewomanhowmuchpermonth = IntegerField('', validators=[Optional()])
    readymadewomanperyear = IntegerField('', validators=[Optional()])

    readymadechildrenhowmuchpermonth = IntegerField('', validators=[Optional()])
    readymadechildrenperyear = IntegerField('', validators=[Optional()])

    materialfabrichowmuchpermonth = IntegerField('', validators=[Optional()])
    materialfabricperyear = IntegerField('', validators=[Optional()])

    furniturehowmuchpermonth = IntegerField('', validators=[Optional()])
    furnitureperyear = IntegerField('', validators=[Optional()])

    equipmenthowmuchpermonth = IntegerField('', validators=[Optional()])
    equipmentperyear = IntegerField('', validators=[Optional()])

    electricalitemhowmuchpermonth = IntegerField('', validators=[Optional()])
    electricalitemperyear = IntegerField('', validators=[Optional()])

    foodhowmuchpermonth = IntegerField('', validators=[Optional()])
    foodperyear = IntegerField('', validators=[Optional()])

    agriculturehowmuchpermonth = IntegerField('', validators=[Optional()])
    agricultureperyear = IntegerField('', validators=[Optional()])

    kerosenehowmuchpermonth = IntegerField('', validators=[Optional()])
    keroseneperyear = IntegerField('', validators=[Optional()])

    taxhowmuchpermonth = IntegerField('', validators=[Optional()])
    taxperyear = IntegerField('', validators=[Optional()])

    festivitieshowmuchpermonth = IntegerField('', validators=[Optional()])
    festivitiesperyear = IntegerField('', validators=[Optional()])

    donationhowmuchpermonth = IntegerField('', validators=[Optional()])
    donationperyear = IntegerField('', validators=[Optional()])

    deposithowmuchpermonth = IntegerField('', validators=[Optional()])
    depositperyear = IntegerField('', validators=[Optional()])

    legalcompenhowmuchpermonth = IntegerField('', validators=[Optional()])
    legalcompenperyear = IntegerField('', validators=[Optional()])

    houserepairhowmuchpermonth = IntegerField('', validators=[Optional()])
    houserepairperyear = IntegerField('', validators=[Optional()])

    lightninghowmuchpermonth = IntegerField('', validators=[Optional()])
    lightningperyear = IntegerField('', validators=[Optional()])

    cookinghowmuchpermonth = IntegerField('', validators=[Optional()])
    cookingperyear = IntegerField('', validators=[Optional()])

    otherhowmuchpermonth = IntegerField('', validators=[Optional()])
    otherperyear = IntegerField('', validators=[Optional()])

    hhclassification  = RadioField('49. How do you classify your HH in relation to others in this village?',
        choices=[('very poor', 'a) very poor'),
            ('poor', 'b) poor'),
            ('average', 'c) average'),
            ('non-poor', 'd) non-poor'),
            ('rich', 'e) rich')], validators=[Optional()])

    hhclassificationreasons = TextAreaField('50. Why do you classify so? Give reasons', render_kw={"rows": 2, "cols": 80})

    villageclassification = RadioField('51. According to your perceptions, is this village',
        choices=[('poor', 'a) poor'),
            ('average', 'b) average'),
            ('rich', 'c) rich')], validators=[Optional()])

    describestrugglingfamilyvillage = TextAreaField('52. How would you describe economically struggling families and villages?', render_kw={"rows": 3, "cols": 80})

    # Environment
    trafficnoiselevel = RadioField('53. Can you describe the state of traffic noise level of the project road? (for control villages, their nearest road)',
        choices=[('high', 'a) high'),
            ('moderate', 'b) moderate'),
            ('low', 'c) low'),
            ('no noise', 'c) no noise')], validators=[Optional()])
    sleepdisturbancefromnoise = RadioField('54. Have you experienced sleep disturbance from noise?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    howlongliveinhouse = IntegerField('55. How long have you lived in your current house (years)?', validators=[Optional()])
    sourceofnoisepollution1 = StringField('1.')
    sourceofnoisepollution2 = StringField('2.')
    sourceofnoisepollution3 = StringField('3.')
    noisedisturbancetime = RadioField('57. What time of the day you usually experience most noise disturbance?',
        choices=[('night', 'a) night (10pm-7am)'),
            ('day', 'b) day (7am-10pm)'),
            ('day night', 'c) both day and night')], validators=[Optional()])

    mainreasonforsoilerosion = TextAreaField('58. In your perceptions, what are the main reasons for soil erosion and sedimentation in your village?', render_kw={"rows": 3, "cols": 80})
    havesoilerosion = RadioField('59. Is there soil erosion around your house or garden?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    reasonforerosion = TextAreaField('If yes, what is the reason for erosion?')

    accesstocleanwater = RadioField('60. Do you have access to clean water?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    getdrinkingwater = StringField('61. Where do you get your drinking water from?')

    riverwaterqualityrainy = RadioField('',
        choices=[('good', '(1) good'),
            ('some contamination', '(2) some contamination'),
            ('very contaminated', '(3) very contaminated')], validators=[Optional()])
    riverwaterqualitydry = RadioField('',
        choices=[('good', '(1) good'),
            ('some contamination', '(2) some contamination'),
            ('very contaminated', '(3) very contaminated')], validators=[Optional()])
    riverwateradequacyrainy = RadioField('',
        choices=[('good', '(1) good'),
            ('some contamination', '(2) some contamination'),
            ('very contaminated', '(3) very contaminated')], validators=[Optional()])
    riverwateradequacydry = RadioField('',
        choices=[('good', '(1) good'),
            ('some contamination', '(2) some contamination'),
            ('very contaminated', '(3) very contaminated')], validators=[Optional()])

    creekwaterqualityrainy= RadioField('',
        choices=[('good', '(1) good'),
            ('moderate', '(2) moderate'),
            ('scarce', '(3) scarce')], validators=[Optional()])
    creekwaterqualitydry = RadioField('',
        choices=[('good', '(1) good'),
            ('moderate', '(2) moderate'),
            ('scarce', '(3) scarce')], validators=[Optional()])
    creekwateradequacyrainy = RadioField('',
        choices=[('good', '(1) good'),
            ('moderate', '(2) moderate'),
            ('scarce', '(3) scarce')], validators=[Optional()])
    creekwateradequacydry = RadioField('',
        choices=[('good', '(1) good'),
            ('moderate', '(2) moderate'),
            ('scarce', '(3) scarce')], validators=[Optional()])

    mainreasonaffectiontoquality = TextAreaField('63. In your perceptions, what are the main reasons affecting to the quality of drinking water?', render_kw={"rows": 3, "cols": 80})

    accesestoairquality = RadioField('64. How do you assess air quality in your village?',
        choices=[('Clean all-year round', 'a) Clean all-year round'),
            ('Clean in some months only', 'b) Clean in some months only'),
            ('Unclean throughout year', 'c) Unclean throughout year')], validators=[Optional()])
    sourceofairpollution1 = StringField('1.')
    sourceofairpollution2 = StringField('2.')
    sourceofairpollution3 = StringField('3.')
    trafficmovementproducedust = RadioField('66. Does the movement of traffic on the road at present produce dust?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    dustlevelofroad = RadioField('67. If yes, describe the dust level of the road/foot path?',
        choices=[('high', 'a) high'),
            ('moderate', 'b) moderate'),
            ('low', 'c) low'),
            ('not at all', 'd) not at all')], validators=[Optional()])
    airpollutioncausehealth = RadioField('68. Does the air pollution cause any health or other problems?',
        choices=[('no problems', 'a) no problems'),
            ('some problems', 'b) some problems'),
            ('significant problems', 'c) significant problems'),
            ('don’t know', 'd) don’t know')], validators=[Optional()])
    airpollutionwhichproblem = StringField('If Yes, which problems?')
    additionalcommentonairquality = TextAreaField('69. Do you have any additional comments or concerns on air quality?', render_kw={"rows": 2, "cols": 80})

    changeinroadsidevegetation = RadioField('70. Has there been a significant change in the roadside vegetation during recent years?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    changeinroadsidevegetationreasons = TextAreaField('If yes, how has it changed?')
    changeinwildanimal = RadioField('71. Has there been a change in the wild animal species which live near the road during recent years?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    changeinwildanimalreasons = TextAreaField('If yes, what kind of changes?')
    humanactivitiesincreased = RadioField('72. Have the human activities (housing, gardening, farming, etc.) increased along the road during recent years?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    kindofhumanactivities = TextAreaField('73. Please specify, what kind of human activities have increased because of the road?', render_kw={"rows": 2, "cols": 80})

    ## Road maintenance and safety

    roadconditionnow = IntegerField('74. Using a scale from 0 (non-motorable) to 5 (excellent), which number would you give to the project road condition?',
        validators=[Optional(), NumberRange(0, 5)])

    roadmaintenancenow = IntegerField('75. Using a scale from 0 (non-existing) to 5 (excellent), which number would you give to the maintenance of the project road (or nearest road in control areas)?',
        validators=[Optional(), NumberRange(0, 5)])

    communitymaintenance = RadioField('76. Does the community participate in maintenance?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])

    paidmaintenance = RadioField('Are they paid for maintenance?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    whopaymaintenance = StringField('If yes, who pays for maintenance work?')
    frequencyofmaintenance = StringField('Frequency of the maintenance work?')
    womenworkonroadmaintenance = RadioField('Have women in this village worked on road maintenance?',
        choices=[('Yes', 'Yes'),
            ('No', 'No')], validators=[Optional()])
    howmanywomenmaintain = StringField('If yes, how many women?')

    # Final Steps
    comments = TextAreaField('Write down below any comments you have.', render_kw={"rows": 2, "cols": 80})
    confirmation = BooleanField('Confirm that you have finished entering all the information before submitting',
        validators=[Required()])

    submit = SubmitField('Submit')
